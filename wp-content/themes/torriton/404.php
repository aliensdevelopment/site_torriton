<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>
<section id="error-page" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/erro-torriton.jpg')" class="foto-bg full-height">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="small-12 medium-12">
				<div class="medium-6">
					<h1 class="titulo-erro text-center"> oops !!!</h1>
					<p class="text-center"><em>Parace que a página que você está buscando não existe!</em></p>
					 <div class="btn-area text-center">
		            	<a class="button button--purple" href="https://beta02.aliensdesign.com.br/torriton">Voltar a Home
		            	<i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
		             </div>
		         </div>
			</div>
		</div>
	</div>
</section>
<?php get_footer();