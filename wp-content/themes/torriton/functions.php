<?php
/**
 * Author: Ole Fredrik Lie
 * URL: http://olefredrik.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Format comments */
require_once( 'library/class-foundationpress-comments.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/class-foundationpress-top-bar-walker.php' );
require_once( 'library/class-foundationpress-mobile-walker.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/class-foundationpress-protocol-relative-theme-assets.php' );

// Registro Widgets
if ( function_exists('register_sidebar') )
{
    register_sidebar(array(
        'name' => __( 'Categorias Torriton'),
        'id' => 'sidebar-1',
        'description' => __( ''),
        'before_title' => '<h1>',
        'after_title' => '</h1>',
    ) );

        register_sidebar(array(
        'name' => __( 'Arquivos Torriton'),
        'id' => 'sidebar-2',
        'description' => __( ''),
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );
}

// add_filter('wpcf7_form_elements', function($content) {
//     $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
//     return $content;
// });

add_filter( 'wpcf7_autop_or_not', '__return_false' );

add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
if (!current_user_can('admin') && !is_admin()) {
  show_admin_bar(false);
}
}

// Remover resultados da busca
function wcs_exclude_category_search( $query ) {
  if ( is_admin() || ! $query->is_main_query() )
    return;

  if ( $query->is_search ) {
    $query->set( 'cat', '-572,-581,-549,-580,-579,-562,-574,-577,-575,-576,-578,-564,-553,-554,-552,-565,-571,-566,-568,-570,-569,-563,-573,599,600,-598,-585,-591,-600,-601,-586,-604' );
  }

}
add_action( 'pre_get_posts', 'wcs_exclude_category_search', 1 );