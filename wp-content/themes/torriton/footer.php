<?php
/**
 * Footer
 */
?>

<footer class="footer error-page_footer">
    <div class="footer-container">
        <div class="footer-grid">
          <div class="footer-grid__wrap-logo-nav">
            <div class="site-desktop-title footer-logo">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                <?php get_template_part('dist/assets/images/inline', 'logo.svg'); ?>
              </a>
            </div>
            <ul class="social">
              <li class="social-item"><a href="https://br.pinterest.com/torriton/" target="_blank"><?php get_template_part('dist/assets/images/inline', 'pinterest.svg'); ?></a></li>
              <li class="social-item"><a href="https://www.youtube.com/user/torritonbeauty" target="_blank"><?php get_template_part('dist/assets/images/inline', 'youtube.svg'); ?></a></li>
              <li class="social-item"><a href="https://www.linkedin.com/company/torriton/" target="_blank"><?php get_template_part('dist/assets/images/inline', 'linkedin.svg'); ?></a></li>
              <li class="social-item"><a href="https://www.facebook.com/torriton/" target="_blank"><?php get_template_part('dist/assets/images/inline', 'facebook.svg'); ?></a></li>
              <li class="social-item"><a href="https://www.instagram.com/torriton/?hl=en" target="_blank"><?php get_template_part('dist/assets/images/inline', 'instagram.svg'); ?></a></li>
            </ul>
          </div>
          <div class="footer-menu grid-x">
            <?php foundationpress_footer(); ?>
            <?php /* if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) === 'topbar' ) : ?>
              <?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
            <?php endif; */ ?>
          </div>
        </div>
    </div>
</footer>

</div><!-- Close off-canvas content -->

<?php get_template_part( 'template-parts/menu-off-canvas' ); ?>

<script>
var locations = [
  [
    'Pátio Batel', -25.443463, -49.291164, 1
  ],
  [
    'Presidente Taunay', -25.436376, -49.284643, 2
  ],
  [
    'Sete de Setembro', -25.445977, -49.290144, 3
  ],
  [
    'Quiosques', -25.443435, -49.291120, 4
  ]
];

function initMap() {
  try{
    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: new google.maps.LatLng(-25.443463, -49.291164),
    disableDefaultUI: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [
      {
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#f5f5f5"
          }
        ]
      }, {
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      }, {
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      }, {
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "color": "#f5f5f5"
          }
        ]
      }, {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#bdbdbd"
          }
        ]
      }, {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#eeeeee"
          }
        ]
      }, {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      }, {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e5e5e5"
          }
        ]
      }, {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      }, {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#ffffff"
          }
        ]
      }, {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#757575"
          }
        ]
      }, {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#dadada"
          }
        ]
      }, {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#616161"
          }
        ]
      }, {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      }, {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#e5e5e5"
          }
        ]
      }, {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#eeeeee"
          }
        ]
      }, {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#c9c9c9"
          }
        ]
      }, {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "color": "#9e9e9e"
          }
        ]
      }
    ]
  });

  if(map){
    var infowindow = new google.maps.InfoWindow();
    var pinpurple = 'localhost:3000/torriton/site_torriton/wp-content/themes/torriton/dist/assets/images/pinpurple.svg';
    var pinbordo = 'localhost:3000/torriton/site_torriton/wp-content/themes/torriton/dist/assets/images/pinbordo.svg';
    var marker, i;

    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        icon: pinpurple
      });

      if (locations[i][3]) {
        icon: pinbordo
      }

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  }
  }catch(e){
    console.log(e);
  }
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDeGkwn-aija7FAPIUij3GdwEzrM9k4Dkg&callback=initMap"></script>

<?php wp_footer(); ?>

</body>
</html>
