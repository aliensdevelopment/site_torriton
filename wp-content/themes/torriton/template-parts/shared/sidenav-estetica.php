<?php
 /*
    Sidebar Estética
 */
?>
<nav id="sidebar-nav">
   <ul class="sidebar-nav-itens">
      <li class="navfixed-item"><a href="#sobre" class="item" data-number="1"><span class="cd-dot"></span><span class="cd-label">Sobre</span></a></li>
      <li class="navfixed-item"><a href="#centro-estetico" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Centro Estético Torriton</span></a></li>
      <li class="navfixed-item"><a href="#servicos" class="item" data-number="3"><span class="cd-dot"></span><span class="cd-label">Serviços</span></a></li>
   </ul>
</nav>
