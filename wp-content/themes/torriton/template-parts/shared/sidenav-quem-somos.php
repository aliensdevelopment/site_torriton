<?php
 /*
    Sidebar Quem Somos
 */
?>
<nav id="sidebar-nav">
   <ul class="sidebar-nav-itens">
      <li class="navfixed-item"><a href="#quem-somos" class="item" data-number="1"><span class="cd-dot"></span><span class="cd-label">Quem somos</span></a></li>
      <li class="navfixed-item"><a href="#visao-missao" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Visão e Missão</span></a></li>
   </ul>
</nav>
