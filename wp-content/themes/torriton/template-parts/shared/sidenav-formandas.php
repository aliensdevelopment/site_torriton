<?php
 /*
    Sidebar Nav Formandas
 */
?>
<nav id="sidebar-nav">
   <ul class="sidebar-nav-itens">
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/formandas/#sobre" class="item" data-number="1"><span class="cd-dot"></span><span class="cd-label">Sobre</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/formandas/#formanda-torriton" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Formanda Torriton</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/formandas/#servicos" class="item" data-number="3"><span class="cd-dot"></span><span class="cd-label">Serviços</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/formandas/inspire-se" class="item" data-number="4"><span class="cd-dot"></span><span class="cd-label">Inspire-se</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/formandas/inspire-se#depoimentos" class="item" data-number="5"><span class="cd-dot"></span><span class="cd-label">Depoimentos</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/formandas/inspire-se#blog" class="item" data-number="6"><span class="cd-dot"></span><span class="cd-label">Blog e Dicas</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/formandas/inspire-se#duvidas" class="item" data-number="7"><span class="cd-dot"></span><span class="cd-label">Dúvidas</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/formandas/inspire-se#consulte" class="item" data-number="8"><span class="cd-dot"></span><span class="cd-label">Consulte os preços</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/formandas/inspire-se#consulte" class="item" data-number="8"><span class="cd-dot"></span><span class="cd-label">Entre em contato</span></a></li>
   </ul>
</nav>
