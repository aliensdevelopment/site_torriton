<?php
 /*
    Sidebar Bistrot
 */
?>
<nav id="sidebar-nav">
   <ul class="sidebar-nav-itens">
      <li class="navfixed-item"><a href="#sobre" class="item" data-number="1"><span class="cd-dot"></span><span class="cd-label">Sobre</span></a></li>
      <li class="navfixed-item"><a href="#cardapio" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Cardápio</span></a></li>
      <li class="navfixed-item"><a href="#blog" class="item" data-number="3"><span class="cd-dot"></span><span class="cd-label">Dicas e Blog</span></a></li>
   </ul>
</nav>
