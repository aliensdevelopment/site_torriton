<?php
 /*
    Sidebar Nav Megahair
 */
?>
<nav id="sidebar-nav">
   <ul class="sidebar-nav-itens">
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/mega-hair/#sobre" class="item" data-number="1"><span class="cd-dot"></span><span class="cd-label">Sobre</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/mega-hair/#mega-torriton" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Megahair Torriton</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/mega-hair/#vantagens" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Vantagens</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/mega-hair/#tecnicas" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Técnicas de Mega Hair</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/mega-hair/antes-e-depois" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Fotos Antes e Depois</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/mega-hair/antes-e-depois#depoimentos" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Depoimentos</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/mega-hair/antes-e-depois#duvidas" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Dúvidas</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/mega-hair/antes-e-depois#blog" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Blog/Dicas</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/mega-hair/antes-e-depois#consulte" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Consulte os preços</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/mega-hair/antes-e-depois#consulte" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Entre em Contato</span></a></li>
   </ul>
</nav>
