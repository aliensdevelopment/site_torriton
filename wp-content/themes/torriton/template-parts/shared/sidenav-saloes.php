<?php
 /*
    Sidebar Salões e Quiosques
 */
?>
<nav id="sidebar-nav">
   <ul class="sidebar-nav-itens">
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/saloes-e-quiosques/saloes-e-quiosques/" class="item" data-number="1"><span class="cd-dot"></span><span class="cd-label">Pátio Batel</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/saloes-e-quiosques/salao-sete-de-setembro/" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Sete de Setembro</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/saloes-e-quiosques/salao-taunay/" class="item" data-number="3"><span class="cd-dot"></span><span class="cd-label">Presidente Taunay</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/saloes-e-quiosques/quiosque-loreal/" class="item" data-number="4"><span class="cd-dot"></span><span class="cd-label">Quiosques</span></a></li>
   </ul>
</nav>
