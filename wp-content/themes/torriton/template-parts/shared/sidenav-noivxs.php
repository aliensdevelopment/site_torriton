<?php
 /*
    Sidebar Nav Noivos e Noivas
 */
?>
<nav id="sidebar-nav">
   <ul class="sidebar-nav-itens">
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/dia-da-noiva-e-do-noivo/#sobre" class="item" data-number="1"><span class="cd-dot"></span><span class="cd-label">Sobre</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/dia-da-noiva-e-do-noivo/#video" class="item" data-number="3"><span class="cd-dot"></span><span class="cd-label">Noivas e Noivos Torriton</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/dia-da-noiva-e-do-noivo/#pacotes-personalizados" class="item" data-number="4"><span class="cd-dot"></span><span class="cd-label">Pacotes Personalizados</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/infraestrutura/" class="item" data-number="5"><span class="cd-dot"></span><span class="cd-label">Infraestrutura</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/infraestrutura/#sala-noiva" class="item" data-number="6"><span class="cd-dot"></span><span class="cd-label">Sala da Noiva</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/hotel-dia-da-noiva/" class="item" data-number="7"><span class="cd-dot"></span><span class="cd-label">Hotel dia da Noiva</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/hotel-dia-da-noiva/#seusconvidados" class="item" data-number="8"><span class="cd-dot"></span><span class="cd-label">Seus Convidados</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/dia-do-noivo/" class="item" data-number="9"><span class="cd-dot"></span><span class="cd-label">Dia do Noivo</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/dia-do-noivo/#consultoria" class="item" data-number="10"><span class="cd-dot"></span><span class="cd-label">Consultoria Gratuita</span></a></li>
     <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/dia-do-noivo/#consulte" class="item" data-number="11"><span class="cd-dot"></span><span class="cd-label">Consulte os preços</span></a></li>
      <li class="navfixed-item"><a href="https://beta02.aliensdesign.com.br/torriton/dia-do-noivo/#consulte" class="item" data-number="12"><span class="cd-dot"></span><span class="cd-label">Entre em contato</span></a></li>
   </ul>
</nav>
