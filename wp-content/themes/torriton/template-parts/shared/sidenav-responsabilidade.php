<?php
 /*
    Sidebar Responsabilidade
 */
?>
<nav id="sidebar-nav">
   <ul class="sidebar-nav-itens">
      <li class="navfixed-item"><a href="#responsabilidade" class="item" data-number="1"><span class="cd-dot"></span><span class="cd-label">Responsabilidade Social</span></a></li>
      <li class="navfixed-item"><a href="#parcerias" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Parcerias</span></a></li>
		<li class="navfixed-item"><a href="#doacao" class="item" data-number="3"><span class="cd-dot"></span><span class="cd-label">Doação de Cabelos</span></a></li>
		<li class="navfixed-item"><a href="#reconhecimento" class="item" data-number="4"><span class="cd-dot"></span><span class="cd-label">Reconhecimento</span></a></li>
   </ul>
</nav>
