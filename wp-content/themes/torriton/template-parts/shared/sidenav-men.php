<?php
 /*
    Sidebar Men
 */
?>
<nav id="sidebar-nav">
   <ul class="sidebar-nav-itens">
      <li class="navfixed-item"><a href="#sobre" class="item" data-number="1"><span class="cd-dot"></span><span class="cd-label">Sobre</span></a></li>
      <li class="navfixed-item"><a href="#servicos" class="item" data-number="2"><span class="cd-dot"></span><span class="cd-label">Serviços</span></a></li>
      <li class="navfixed-item"><a href="#inspire-se" class="item" data-number="3"><span class="cd-dot"></span><span class="cd-label">Inspire-se</span></a></li>
      <li class="navfixed-item"><a href="#blog" class="item" data-number="4"><span class="cd-dot"></span><span class="cd-label">Dicas e Blog</span></a></li>
   </ul>
</nav>
