<?php
 /*
   Depoimentos
 */
?>

<div id="depoimentos" class="parallax-c cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/formandas/depoimentos.jpg"></div>
<div class="frost-wrapper"></div>
  <div class="wrap-info-fullhero c grid-container wrap-block-side depoimentos full-height">
    <div class="grid-x grid-margin-x align-right block-content">
      <div class="small-12 medium-9">
        <h1 data-aos="fade-right" class="small-5 branco cell title-big">
          DEPOIMENTOS
        </h1>

        <div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
          <div class="box-info-int full-width box-info-dica cell medium-4 small-12">
            <span class="line"></span>
            <h2 class="title">Confira dos depoimentos de formandas que já tiveram seu dia produzido pelo Torriton!</h2>
          </div>

          <div class="cell small-12">
            <div class="depoimentos-carousel owl-carousel owl-theme">

              <!-- Depoimento Formandas -->
              <?php

                global $post;
                $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'depoimento-formandas' );

                $myposts = get_posts( $args );

                foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                  <div class="depoimentos-item">
                    <div class="text dica">
                      <p><?php the_field('texto_depoimento'); ?></p>
                      <legend class="title"><?php the_field('nome_formanda'); ?></legend>
                    </div>
                  </div>

                <?php endforeach; 
                wp_reset_postdata();?>

            </div>
          </div>

          <div class="grid-x full-width box-info-links align-right">
            <div class="cell small-5">
              <div class="btn-area">
                <a class="button button--formandas" href="#consulte">Entre em contato
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>

                <a class="button button--formandas" href="#consulte">Consulte os preços
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
