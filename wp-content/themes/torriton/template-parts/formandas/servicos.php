<?php
  /*
    Servicos
  */
?>

 <section id="servicos" class="itens-wrap servicos grid-container">
    <div class="grid-x grid-margin-x align-right">
    <div class="small-9">
      <h3 class="t-info-title int">SERVIÇOS</h3>
      <div class="grid-x itens-wrap-int align-justify">

        <?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'servicos-formandas' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

            <?php if( get_field('titulo_servico') ): ?>
                <article data-aos="fade-up" data-aos-offset="250" data-aos-duration="1000" class="item-block cell noivas">
                    <header class="item-block-title formandas"><?php the_field("titulo_servico"); ?></header>

                    <!-- Verifica imagem -->
                    <?php 
                        $image = get_field('foto_destaque');

                        if( !empty($image) ): ?>

                            <a href="#" data-open="modalNoivas">
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="item-block-foto" /></a>

                        <?php endif; ?>
                <?php endif; ?>

                    <!-- Verifica campo vazio -->
                    <?php if( get_field('descricao_do_servico') ): ?>
                        <div class="item-block-text">
                            <!-- Campo personalizado -->
                            <?php the_field("descricao_do_servico"); ?>
                        </div>
                    <?php endif; ?>

                    <?php if( get_field('box_destaque') ): ?>
                        <div class="destaque-area">
                            <p>
                              <?php the_field("box_destaque"); ?>
                            </p>
                        </div>
                    <?php endif; ?>

                    <?php if( get_field('botao_1') ): ?>
                        <div class="btn-area">
                        <a class="button button--formandas" href="<?php the_field("url_btn_1"); ?>"><?php the_field("botao_1"); ?>
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
                        </div> 
                    <?php endif; ?>

                    <?php if( get_field('botao_2') ): ?>
                        <div class="btn-area">
                        <a class="button button--formandas" href="<?php the_field("url_btn_2"); ?>"><?php the_field("botao_2"); ?>
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
                        </div>
                    <?php endif; ?>

                    <?php if( get_field('detalhes_adicionais') ): ?>
                        <div class="item-block-text">
                            <?php the_field("detalhes_adicionais"); ?>
                        </div>
                    <?php endif; ?>

                </article>
            <?php endforeach; 
            // Fim do Laço
            wp_reset_postdata();?>

        </div>
    </div>
    </div>
 </section>
