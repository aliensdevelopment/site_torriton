<?php
 /*
   Formandas Torriton
 */
?>

<section id="formanda-torriton" class="wrap-frost-spacing">
  <div class="grid-container full wrap-block-side transparent">
    <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/formandas/formanda.jpg')" class="foto-bg"></div>

    <div class="grid-x grid-margin-x align-center block-content">
      <h1 data-aos="fade-right" class="title-big">
        FORMANDA TORRITON
      </h1>

      <article class="box-info-int box-info-int-white cell small-6">
        <span class="line"></span>
          <div class="text">
          <p>Desde 1977 escolhemos, apoiamos e formamos as melhores equipes para atender as formandas no Torriton.</p>
          <p>
          O nosso pacote para formandos para um, dois ou três dias – colação de grau, missa e baile de formatura - combina serviços essenciais de maquiagem para formatura, lavado e escova modelada e penteado para formatura / baile.
          </p>

          </div>
          <h2 class="title">O melhor salão de beleza para formandos!</h2>
          <br/>
          <a href="#consulte" class="button button--formandas">Entre em contato
            <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i>
          </a>
          <br/>
          <a href="#consulte" class="button button--formandas">Consulte os preços
            <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i>
          </a>
      </article>
    </div>
  </div>
</section>
