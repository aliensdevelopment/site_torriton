<?php
 /*
  Hero
 */
?>

<header class="hero-int" role="banner">
	<div id="sobre" data-aos="fade-right" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/formandas/formandas-wrap.jpg')" class="foto-bg"></div>
	<section class="conteudo grid-x align-right">
        <h1 data-aos="fade-left" data-aos-duration="1200" class="title-big">
          FORMANDAS
				</h1>
				<div class="box-info-int cell small-12 medium-6">
					<h2 class="title">SOBRE</h2>
					<div class="text">
						<p>O momento tão esperado chegou e é todo seu! Foram longos anos de esforço e dedicação para o dia da formatura chegar, mas logo as luzes e os olhares vão estar em você e finalmente estará formada!</p>
						<p>Seja na colação de grau, na missa ou no baile de formatura; as formandas merecem estar lindas e nada mais adequado do que um pacote para formatura exclusivo e especial somente para vocês, com serviços essenciais realizados com os melhores profissionais, assim você não precisará se preocupar com nada além de curtir muito!</p>
					</div>
				</div>
	</section>
</header>
