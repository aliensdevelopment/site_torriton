
<?php
/*
	Torrion Estético e Serviços
*/

?>
<section id="servicos">
	<div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/estetica/torriton-estetica-03.jpg')" class="foto-bg estetico-servicos-bg"></div>
	<div class="wrap-info-fullhero e grid-container wrap-block-side full-height">
		<div class="grid-x grid-margin-x align-right block-content">
			<div class="small-9 estetico-servicos">
				<h1 class="t-title-diamond small-5 cell estetico-servicos-title">
					SERVIÇOS
				</h1>
				<div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
					<div class="cell small-12 box-infosub-int">
						<h2 class="title t-info-title">
							Estética Corporal | Estética Facial | Podologia e mãos | Depilação
						</h2>
						<a href="#" class="button button--purple">Entre em Contato <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

