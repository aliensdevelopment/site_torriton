<?php
 /*
    Blog Carrosel - Torriton Estética
 */
?>

<section data-aos="fade-up" data-aos-offset="100" data-aos-duration="900" class="blog-int grid-container full">
	<div class="grid-x grid-margin-x align-right block-content">
		<div class="small-12 medium-9">
			<div class="grid-container">
	 			<div class="blog-wrap grid-x grid-margin-x">
	 				<div class="box-info block blog block-store cell small-12 medium-9 large-6">
	 					<article class="grid-x grid-margin-x">
	 						<div class="cell small-6">
	 							<div class="text">
	 								<h2 class="title-dicas"> Dicas e Blog
									</h2>
									<span class="line-blog"></span>
	 							</div>
	 							<div class="text">
	 								<p>Tá esperando o que para viver essa experiência?
	 									<br/>
										Marque já o seu horário!
									</p>
	 							</div>
	 						</div>

							<div class="newsletter-blog ">
								<div class="box-info block blog small-4">
									<div class="matter-block">
										<article class="grid-x grid-margin-x">
											<div class="cell small-12">
												<div class="text">
													<p>Cadastre-se na newsletter e receba dicas inscríveis para o seu dia! </p>
												</div>
													<form>
												      <div class="small-12 cell">
												        <label>
												          <input type="text" placeholder="Nome:">
												        </label>
												      </div>
												      <div class="small-12 cell">
												        <label>
												          <input type="text" placeholder="E-mail:">
												        </label>
												      </div>
															<button class="submit item-block-link"><input type="submit" value="Enviar"><i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></button>
														</form>
											</div>
										</article>
									</div>
								</div>
							</div>
	 					</article>
	 				</div>
	 			</div>
	 		</div>

		    <!-- Blog -->
				<?php
		  			$blog_posts = new WP_Query(
				    array(
				      'post_type' => 'post',
				      'posts_per_page' => 6,
							'paged' => get_query_var('paged'),
							'category_name' => 'estetica'
						    )
				  ); ?>

			 		<div class="blog-carousel owl-carousel owl-theme">

			 		<?php if ( $blog_posts->have_posts() ) : ?>
		            	<?php while ( $blog_posts->have_posts() ) : $blog_posts->the_post(); ?>

					<div class="blog-carousel-item">

						<header><a  href="<?php the_permalink(); ?>"><?php getTheFirstImage(); ?></a></header>
						<article class="content">
							<span class="date"><?php the_time('j \d\e F \d\e Y') ?></span>
							<a href="<?php the_permalink(); ?>"><p class="resume"><?php the_title(); ?></p></a>
						</article>
					</div>

		            <?php endwhile;?>
		        <?php endif; ?>

	 		</div>
		</div>
	</div>
</section>
