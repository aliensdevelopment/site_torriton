<?php
 /*
   Centro Estético Torriton
 */
?>

<section id="centro-estetico">
  <div class="parallax-c cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/estetica/torriton-estetica-02.jpg"></div>
  <div class="wrap-info-fullhero c grid-container wrap-block-side full-height">
    <div class="grid-x grid-margin-x align-right block-content estetico-torriton-centro">
      <div class="small-9">
        <h1 data-aos="fade-right" class="small-5 branco cell t-title-diamond">
          CENTRO ESTÉTICO DO TORRITON
        </h1>

        <div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
          <div class="box-info-int full-width box-info-dica cell medium-8 small-12">
            <p class="t-info-text">
            O Centro Estético do Torriton conta com consultoria de esteticistas renomadas, profissionais com formação no Brasil e no exterior, licenciadas pelas melhores academias de estética do mundo. Oferecemos os melhores serviços de estética e cosmética. 
            </p>
            <a href="#" class="button button--purple">Entre em Contato <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
