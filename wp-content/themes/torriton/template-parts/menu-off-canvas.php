<?php
/**
 * Menu off-canvas
 */
?>

<div class="off-canvas position-right" id="offCanvasRight" data-off-canvas role="navigation">
  <?php foundationpress_off_canvas(); ?>

  <ul class="social">
    <li class="social-item"><a href="https://br.pinterest.com/torriton/" target="_blank"><?php get_template_part('dist/assets/images/inline', 'pinterest.svg'); ?></a></li>
    <li class="social-item"><a href="https://www.youtube.com/user/torritonbeauty" target="_blank"><?php get_template_part('dist/assets/images/inline', 'youtube.svg'); ?></a></li>
    <li class="social-item"><a href="https://www.linkedin.com/company/torriton/" target="_blank"><?php get_template_part('dist/assets/images/inline', 'linkedin.svg'); ?></a></li>
    <li class="social-item"><a href="https://www.facebook.com/torriton/" target="_blank"><?php get_template_part('dist/assets/images/inline', 'facebook.svg'); ?></a></li>
    <li class="social-item"><a href="https://www.instagram.com/torriton/?hl=en" target="_blank"><?php get_template_part('dist/assets/images/inline', 'instagram.svg'); ?></a></li>
  </ul>
</div>
