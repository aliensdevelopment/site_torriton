<?php
 /*
    Blog Carrosel Single
 */
?>

<section data-aos="fade-up" data-aos-offset="100" data-aos-duration="900" class="blog-int grid-container full">
	<div class="grid-x grid-margin-x align-center block-content">

		<div class="grid-container comentarios">
			<div class="grid-x grid-margin-x">
				<div class="cell small-12">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php comments_template(); ?>
					<?php endwhile; ?>
				</div>
			</div>
		</div>

		<div class="small-12">
			<div class="grid-container">
	 			<div class="blog-wrap grid-x grid-margin-x">
	 				<div class="box-info block-b block blog cell small-6">
						<span class="line line-branco"></span>
	 					<h3 class="title">VOCÊ TAMBÉM
						PODE GOSTAR</h3>
	 				</div>
	 			</div>
	 		</div>

	 		<div class="blog-carousel no-spacing owl-carousel owl-theme">

			<?php
				$args = array(
					'posts_per_page' => 6,
					'post__not_in'   => array( get_the_ID() ),
					'no_found_rows'  => true,
				);

				//Same category
				$cats = wp_get_post_terms( get_the_ID(), 'category' ); 
				$cats_ids = array();  
				foreach( $cats as $wpex_related_cat ) {
					$cats_ids[] = $wpex_related_cat->term_id; 
				}

				if ( ! empty( $cats_ids ) ) {
					$args['category__in'] = $cats_ids;
				}

				// Query posts
				$wpex_query = new wp_query( $args );

				foreach( $wpex_query->posts as $post ) : setup_postdata( $post ); ?>
					<div class="blog-carousel-item">
						<a href="<?php the_permalink(); ?>"><header style=" background-image: url('<?php the_post_thumbnail_url('full'); ?>');" class="thumbnail"></header></a>
							<article class="content">
			 						<span class="date"><?php the_time('j \d\e F \d\e Y') ?></span>
			 						<a href="<?php the_permalink(); ?>"><p class="resume"><?php the_title(); ?></p></a>
			 				</article>
			 		</div>
				<?php
				endforeach;
				wp_reset_postdata(); ?>
	 		</div>
		</div>
	</div>

</section>
