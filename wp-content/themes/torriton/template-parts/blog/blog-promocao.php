<?php
 /*
    Blog Carrosel - Confira as Promoções
 */
?>
<div id="blog">
<?php //ID para ancoragem ?>
</div>
<section id="modulo-blog" data-aos="fade-up" data-aos-offset="100" data-aos-duration="900" class="blog-int grid-container full">
	<div class="grid-x grid-padding-x align-right block-content">
		<div class="small-12 medium-9 blog-pos">
			<div class="grid-container">
	 			<div class="blog-wrap grid-x grid-margin-x">
	 				<div class="box-info block blog cell small-12 medium-9 large-7">
	 					<article class="grid-x grid-margin-x">
	 						<div class="cell small-6">
	 							<div class="text">
	 								<h2 class="title-dicas"> Dicas e Blog
									</h2>
									<span class="line-blog"></span>
	 							</div>
	 							<div class="text">
	 								<p>Tá esperando o que para viver essa experiência?
	 									<br/>
										Marque já o seu horário!
									</p>
									<a style="margin-bottom: 2em;" class="t-info-text link" href="https://beta02.aliensdesign.com.br/torriton/promocoes/">Conheça as promoções </a>
	 							</div>
	 						</div>

							<div class="newsletter-blog ">
								<div class="box-info block blog small-4">
									<div class="matter-block">
										<article class="grid-x grid-margin-x">
											<div class="cell small-12">
												<div class="text">
													<p>Cadastre-se na newsletter e receba dicas inscríveis para o seu dia! </p>
												</div>
													<form>
												      <div class="small-12 cell">
												        <label>
												          <input type="text" placeholder="Nome:">
												        </label>
												      </div>
												      <div class="small-12 cell">
												        <label>
												          <input type="text" placeholder="E-mail:">
												        </label>
												      </div>
															<button class="submit item-block-link"><input type="submit" value="Enviar"><i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></button>
														</form>
											</div>
										</article>
									</div>
								</div>
							</div>
	 					</article>
	 				</div>
	 			</div>
	 		</div>

		    <!-- Blog -->
				<?php
		  			$blog_posts = new WP_Query(
				    array(
				      'post_type' => 'post',
				      'posts_per_page' => 6,
							'paged' => get_query_var('paged'),
							'category_name' => 'blog'
						    )
				  ); ?>

			 		<div class="blog-carousel owl-carousel owl-theme">

			 		<?php if ( $blog_posts->have_posts() ) : ?>
		            	<?php while ( $blog_posts->have_posts() ) : $blog_posts->the_post(); ?>

					<div class="blog-carousel-item">

						<a href="<?php the_permalink(); ?>"><header class="thumbnail" style=" background-image: url('<?php the_post_thumbnail_url('full'); ?>');" >
						</header></a>
						<article class="content">
							<span class="date"><?php the_time('j \d\e F \d\e Y') ?></span>
							<a href="<?php the_permalink(); ?>"><p class="resume"><?php the_title(); ?></p></a>
						</article>
					</div>

		            <?php endwhile;?>
		        <?php endif; ?>

	 		</div>
		</div>
	</div>
</section>
