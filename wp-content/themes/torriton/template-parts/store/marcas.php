<?php
 /*
    Marcas
 */
?>

<section id="marcas">

  <!-- Galeria Marcas -->
  <div class="grid-container full wrap-block-side full-height">
    <div class="grid-container">
      <div class="grid-x grid-margin-x">
        <div class=" small-12 medium-12">
          <div class="grid-x grid-padding-x ">
            <div class="medium-12 medium-offset-4 align-right">
              <div id="" class="bloco-marcas" >
                  <span class="purple-line"></span>
                  <p class="t-info-text">
                     Conheça as marcas
                  </p>
                </div>

                  <div class="galeria g-marcas carrosel owl-carousel owl-theme ">

                    <?php
                        global $post;
                        $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-marcas' );

                        $myposts = get_posts( $args );

                        // Início do Laço selecionado pelo nome da categoria
                        foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                            <?php 

                            $images = get_field('galeria');

                            if( $images ): ?>
                                    <?php foreach( $images as $image ): ?>
                                        <div class="marca-item">
                                          <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                                         </div>
                                    <?php endforeach; ?>
                            <?php endif; ?>

                        <?php endforeach; 
                        // Fim do Laço
                      wp_reset_postdata();?>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
