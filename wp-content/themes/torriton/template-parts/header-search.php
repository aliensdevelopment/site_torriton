<?php
 /*
    Barra de Pesquisa Header
 */
?>

 <form role="search" method="get" id="search-form" action="<?php echo home_url( '/' ); ?>">
 	<div class="input-group search-header">
 		<input type="text" class="input-group-field search-header-input" value="" name="s" id="s" placeholder="O que você quer encontrar?">
 		<div class="input-group-button search-header-submit">
      <a type="submit" class="icon-submit" id="search-submit"><?php get_template_part('dist/assets/images/inline', 'search.svg'); ?></a>
 		</div>
 	</div>
 </form>
