<?php
 /*
    Mapa - Locais
 */
?>

<div id="map"></div>

<div class="locais-blocks grid-container">
	<div class="grid-x grid-margin-x">
    <div class="cell small-12 medium-4 local-block">
			<h2 class="title">PÁTIO BATEL</h2>
			<a href="tel:+413030-7676"><i class="locais-icon"><?php get_template_part('dist/assets/images/inline', 'tel.svg'); ?></i><small>+55 (41) 3030-7676</small></a>
			<div class="locais-item"><i class="locais-icon"><?php get_template_part('dist/assets/images/inline', 'clock.svg'); ?></i>
				<p>De segunda a sábado, das 10h às 22h.
				Domingos e feriados, das 14h às 20h.</p>
			</div>
			<div class="locais-item"><i class="locais-icon icon-marker"><?php get_template_part('dist/assets/images/inline', 'marker.svg'); ?></i>
				<p>Av. Batel, 1.868, Piso L2 – Batel
				Curitiba – PR – 80420-090</p>
			</div>
		</div>

		<div class="cell small-12 medium-4 local-block">
			<h2 class="title">PRESIDENTE TAUNAY</h2>
			<a href="tel:+413030-7676"><i class="locais-icon"><?php get_template_part('dist/assets/images/inline', 'tel.svg'); ?></i><small>+55 (41) 3091-8686</small></a>
			<div class="locais-item"><i class="locais-icon"><?php get_template_part('dist/assets/images/inline', 'clock.svg'); ?></i>
				<p>Segunda-feira: das 09:00 as 20:00
				De terça-feira até sábado: das 09:00 as 21:00
				</p>
			</div>
			<div class="locais-item"><i class="locais-icon icon-marker"><?php get_template_part('dist/assets/images/inline', 'marker.svg'); ?></i>
				<p>Al. Pres. Taunay, 321 – Batel
					Curitiba – PR - 80420-180</p>
			</div>
		</div>

		<div class="cell small-12 medium-4 local-block">
			<h2 class="title">SETE DE SETEMBRO</h2>
			<a href="tel:+413030-7676"><i class="locais-icon"><?php get_template_part('dist/assets/images/inline', 'tel.svg'); ?></i><small>+55 (41) 3242-3963</small></a>
			<div class="locais-item"><i class="locais-icon"><?php get_template_part('dist/assets/images/inline', 'clock.svg'); ?></i>
				<p>De segunda-feira até sábado: das 09:00 as 20:30</p>
			</div>
			<div class="locais-item"><i class="locais-icon icon-marker"><?php get_template_part('dist/assets/images/inline', 'marker.svg'); ?></i>
				<p>Av. Sete de Setembro, 5402 – Batel
					Curitiba – PR – CEP 80240-000</p>
			</div>
		</div>

		<div class="cell small-12 local-block big">
			<h2 class="title">QUIOSQUES</h2>
			<div class="locais-item"><i class="locais-icon icon-marker"><?php get_template_part('dist/assets/images/inline', 'marker.svg'); ?></i>
				<p>Quiosque L’oréal Professional
					Shopping Pátio Batel – Piso L2</p>
			</div>
		</div>
  </div>
</div>
