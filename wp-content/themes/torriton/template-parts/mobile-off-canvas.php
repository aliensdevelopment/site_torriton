<?php
/**
 * Template part for off canvas menu
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

<nav class="mobile-off-canvas-menu off-canvas position-left" id="offCanvasLeft" data-off-canvas role="navigation">
	<?php foundationpress_mobile_nav(); ?>
</nav>
