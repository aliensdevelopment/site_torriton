<div id="saloesequiosques" class="subpage">
  <section class="container-spacing">
    <div class="grid-container full wrap-block-side">
      <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/saloes-quiosques/quiosque.jpg')" class="foto-bg"></div>
      <div class="grid-container wrap-block-side spacing transparent full-height">
        <div class="grid-x grid-margin-x align-center block-content spacing-top">
          <div class="small-12 medium-7">
            <div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
              <div class="box-info-int box-info-dica cell small-12">
                <h2 class="title t-info-title m-reset">SALÕES E QUIOSQUES</h2>
                <h3 class="t-title-diamond margin-b medium">QUIOSQUE L’ORÉAL</h3>
                <p class="t-info-unidade">Desde 1909 a venda dos produtos profissionais L’Oréal foi exclusiva dos melhores salões de beleza no mundo inteiro. </p>
                <p class="t-info-text">Em 2017 pela primeira vez na sua história, a L’Oréal desenvolveu um projeto de comercialização de varejo das marcas profissionais Kérastase, Redken e L`Oréal Professionel em quiosques localizados nos melhores shoppings do Brasil e gerenciados pelos próprios salões de beleza.
                Além da venda dos produtos, as clientes contam inclusive com a consultoria de vendedoras especializadas e constantemente treinadas pelo departamento de treinamento L’Oréal.
                Acessibilidade, lançamentos em primeira mão, consultoria especializada e garantia de procedência são os diferenciais desta iniciativa que está rapidamente conquistando o favor do público.
                </p>
                <p class="t-info-text">O Quiosque L’oréal Professional está localizado no Shopping Pátio Batel – Piso L2.</p>
                <a href="http://www.patiobatel.com.br/lojas/260" class="item-block-link">Veja a localização exata do Quiosque <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="galeria-quiosque" data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">
     <div class="grid-container">
        <div class="galeria-wrap grid-x grid-margin-x align-right">
          <div class="small-9">
            <div class="box-info large size-m cell">
              <span class="line"></span>
              <h3 class="title">GALERIA QUIOSQUES</h3>
            </div>
          </div>
        </div>
      </div>

      <div class="galeria int carrosel owl-carousel owl-theme">
        <?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-quiosque' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                <?php 

                $images = get_field('galeria');

                if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                            <div class="galeria-item">
                              <img onclick="galeriaModal(this)" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />
                             </div>
                        <?php endforeach; ?>
                <?php endif; ?>

            <?php endforeach; 
            // Fim do Laço
          wp_reset_postdata();?>
      </div>
  </section>

  <?php get_template_part( 'template-parts/galerias/galeria-foto' ); ?>
</div>