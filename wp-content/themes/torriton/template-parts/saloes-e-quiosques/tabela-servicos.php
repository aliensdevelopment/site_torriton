<?php

/* Modal com Tabelas de Serviços por Unidade */

?>

  <div class="reveal" id="modalServicos" data-reveal >
    <button class="close-button" data-close aria-label="Close modal" type="button">
      <span aria-hidden="true">&times;</span>
    </button>

    <div class="grid-container">
      <div class="grid-x grid-padding-x block-content">
        <div class="medium-12">
          <h2 class="t-title-diamond title-promocoes">Serviços fornecidos torriton</h2>
          <ul class="tabs locais" data-tabs id="servicos-torriton">
            <li class="tabs-title is-active local-title"><a href="#panel1" class="no-anchor" aria-selected="true"><i ><?php get_template_part('dist/assets/images/icon', 'make.svg'); ?></i> Maquiagem</a></li>

            <li class="tabs-title local-title"><a data-tabs-target="panel2" class="no-anchor" href="#panel2"><i ><?php get_template_part('dist/assets/images/icon', 'cabelo.svg'); ?></i> Cabelo</a></li>

            <li class="tabs-title local-title"><a data-tabs-target="panel3" class="no-anchor" href="#panel3"><i ><?php get_template_part('dist/assets/images/icon', 'maos.svg'); ?></i> Mãos e pés</a></li>

            <li class="tabs-title local-title"><a data-tabs-target="panel4" class="no-anchor" href="#panel4"><i ><?php get_template_part('dist/assets/images/icon', 'estetica.svg'); ?></i> Estética</a></li>
          </ul>

          <!-- Início Conteúdo Tabs -->
          <div class="tabs-content tabs-locais" data-tabs-content="servicos-torriton">

            <div class="tabs-panel is-active" id="panel1">
              <!-- Exibe a Tabela  Maquiagem-->
              <?php 

              $table = get_field( 'tabela_maquiagem' );

                if ( $table ) {

                    echo '<table border="0">';

                        if ( $table['header'] ) {

                            echo '<thead>';

                                echo '<tr>';

                                    foreach ( $table['header'] as $th ) {

                                        echo '<th class="th-title">';
                                            echo $th['c'];
                                        echo '</th>';
                                    }

                                echo '</tr>';

                            echo '</thead>';
                        }

                        echo '<tbody>';

                            foreach ( $table['body'] as $tr ) {

                                echo '<tr>';

                                    foreach ( $tr as $td ) {

                                        echo '<td class="td-text">';
                                            echo $td['c'];
                                        echo '</td>';
                                    }

                                echo '</tr>';
                            }

                        echo '</tbody>';

                    echo '</table>';
                }

              ?>

            </div>

            <div class="tabs-panel" id="panel2">
              <!-- Exibe a Tabela  Cabelo-->
              <?php 

              $table = get_field( 'tabela_cabelo' );

                if ( $table ) {

                    echo '<table border="0">';

                        if ( $table['header'] ) {

                            echo '<thead>';

                                echo '<tr>';

                                    foreach ( $table['header'] as $th ) {

                                        echo '<th class="th-title">';
                                            echo $th['c'];
                                        echo '</th>';
                                    }

                                echo '</tr>';

                            echo '</thead>';
                        }

                        echo '<tbody>';

                            foreach ( $table['body'] as $tr ) {

                                echo '<tr>';

                                    foreach ( $tr as $td ) {

                                        echo '<td class="td-text">';
                                            echo $td['c'];
                                        echo '</td>';
                                    }

                                echo '</tr>';
                            }

                        echo '</tbody>';

                    echo '</table>';
                }

              ?>
            </div>

            <div class="tabs-panel" id="panel3">
              <!-- Exibe a Tabela  Homem-->
              <?php 

              $table = get_field( 'tabela_homem' );

                if ( $table ) {

                    echo '<table border="0">';

                        if ( $table['header'] ) {

                            echo '<thead>';

                                echo '<tr>';

                                    foreach ( $table['header'] as $th ) {

                                        echo '<th class="th-title">';
                                            echo $th['c'];
                                        echo '</th>';
                                    }

                                echo '</tr>';

                            echo '</thead>';
                        }

                        echo '<tbody>';

                            foreach ( $table['body'] as $tr ) {

                                echo '<tr>';

                                    foreach ( $tr as $td ) {

                                        echo '<td class="td-text">';
                                            echo $td['c'];
                                        echo '</td>';
                                    }

                                echo '</tr>';
                            }

                        echo '</tbody>';

                    echo '</table>';
                }

              ?>
            </div>

            <div class="tabs-panel" id="panel4">
              <!-- Exibe a Tabela  Mãos e Pés-->
              <?php 

              $table = get_field( 'tabela_maosepes' );

                if ( $table ) {

                    echo '<table border="0">';

                        if ( $table['header'] ) {

                            echo '<thead>';

                                echo '<tr>';

                                    foreach ( $table['header'] as $th ) {

                                        echo '<th class="th-title">';
                                            echo $th['c'];
                                        echo '</th>';
                                    }

                                echo '</tr>';

                            echo '</thead>';
                        }

                        echo '<tbody>';

                            foreach ( $table['body'] as $tr ) {

                                echo '<tr>';

                                    foreach ( $tr as $td ) {

                                        echo '<td class="td-text">';
                                            echo $td['c'];
                                        echo '</td>';
                                    }

                                echo '</tr>';
                            }

                        echo '</tbody>';

                    echo '</table>';
                }

              ?>
            </div>

            <div class="tabs-panel" id="panel5">
              <!-- Exibe a Tabela  Estética-->
              <?php 

              $table = get_field( 'tabela_estetica' );

                if ( $table ) {

                    echo '<table border="0">';

                        if ( $table['header'] ) {

                            echo '<thead>';

                                echo '<tr>';

                                    foreach ( $table['header'] as $th ) {

                                        echo '<th class="th-title">';
                                            echo $th['c'];
                                        echo '</th>';
                                    }

                                echo '</tr>';

                            echo '</thead>';
                        }

                        echo '<tbody>';

                            foreach ( $table['body'] as $tr ) {

                                echo '<tr>';

                                    foreach ( $tr as $td ) {

                                        echo '<td class="td-text">';
                                            echo $td['c'];
                                        echo '</td>';
                                    }

                                echo '</tr>';
                            }

                        echo '</tbody>';

                    echo '</table>';
                }

              ?>
            </div>


          </div>
        </div>
      </div>
    </div>
  </div>