<?php
/* Conteúdo Unidade Sete de Setembro */
?>

<div id="saloesequiosques" class="subpage">
  <section class="container-spacing">
    <div class="grid-container full wrap-block-side">
      <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/saloes-quiosques/sete-de-setembro.jpg')" class="foto-bg"></div>
      <div class="grid-container wrap-block-side spacing transparent full-height">
        <div class="grid-x grid-margin-x align-center block-content spacing-top">
          <div class="small-12 medium-7">
            <div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
              <div class="box-info-int box-info-dica cell small-12">
                <h2 class="title t-info-title m-reset">SALÕES E QUIOSQUES</h2>
                <h3 class="t-title-diamond margin-b medium">SETE DE SETEMBRO</h3>
                <p class="t-info-unidade">A Unidade Sete de Setembro é referência no ramo de salões de beleza práticos em Curitiba.</p>
                <p class="t-info-text">Conta com uma estrutura simples e ao mesmo tempo elegante. Aqui você encontra todos os serviços de beleza e estética, com profissionais renomados em um ambiente super clean. </p>
                <a href="https://beta02.aliensdesign.com.br/torriton/servicos/" target="_blank" class="item-block-link">Confira aqui todos os serviços fornecidos no Torriton Sete de Setembro. <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
                <a href="https://beta02.aliensdesign.com.br/torriton/facilidades/" target="_blank" class="item-block-link">Confira aqui o comparativo de todas as unidades <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <div class="hero-tour cd-section parallax-d" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/saloes-quiosques/hero-sete-de-setembro.jpg">
    <div class="noivas-video-content grid-container full">
      <div class="grid-x grid-margin-x align-middle align-center">
        <a class="cell small-12 tour-btn" href="http://www.torriton.com.br/tour360/setedesetembro/tor3tour.html" target="_blank">TOUR VIRTUAL 360º</a>
      </div>
    </div>
  </div>

  <section data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">
     <div class="grid-container">
        <div class="galeria-wrap grid-x grid-margin-x align-right">
          <div class="small-9">
            <div class="box-info large size-m cell">
              <span class="line"></span>
              <h3 class="title">GALERIA SETE</h3>
            </div>
          </div>
        </div>
      </div>

      <div class="galeria carrosel owl-carousel owl-theme">
        <?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-sete' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                <?php 

                $images = get_field('galeria');

                if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                            <div class="galeria-item">
                              <img onclick="galeriaModal(this)" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />
                             </div>
                        <?php endforeach; ?>
                <?php endif; ?>

            <?php endforeach; 
            // Fim do Laço
          wp_reset_postdata();?>
      </div>
  </section>
  <?php get_template_part( 'template-parts/galerias/galeria-foto' ); ?>

  <section class="itens-wrap servicos grid-container">
   <div class="grid-x grid-margin-x align-right">
     <div class="small-9">
       <h3 class="t-info-title int">NOSSOS SERVIÇOS</h3>
       <div class="grid-x itens-wrap-int align-justify">

            <?php
              global $post;
              $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'unidade-sete-de-setembro' );

              $myposts = get_posts( $args );

              // Início do Laço selecionado pelo nome da categoria
              foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                  <article data-aos="fade-up" data-aos-offset="200" data-aos-duration="1000" class="item-block cell">
                      <header class="item-block-title purple"><?php the_field("titulo_servico_sete_setembro"); ?></header>

                      <?php 
                          $image = get_field('foto_servico_sete_setembro');

                          if( !empty($image) ): ?>

                              <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="item-block-foto" />

                          <?php endif; ?>

                      <div class="item-block-text">
                          <p><?php the_field("descricao_servico_sete_setembro"); ?></p>
                      </div>

                      <div class="item-block-text">
                          <a class="item-block-link" href="<?php the_field("link_servico_sete_setembro"); ?>"><?php the_field("texto_link_sete_setembro"); ?>
                          <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
                      </div>

                  </article>
              <?php endforeach; 
              // Fim do Laço
              wp_reset_postdata();?>

        </div>
     </div>
   </div>
  </section>
</div>