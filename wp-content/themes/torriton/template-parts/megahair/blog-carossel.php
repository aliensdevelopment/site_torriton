<?php
 /*
    Blog Carrosel
 */
?>

<section data-aos="fade-up" data-aos-offset="100" data-aos-duration="900" class="blog-int grid-container full">

	<div class="grid-x grid-margin-x align-right block-content">
		<div class="small-9">
			<div class="grid-container">
	 			<div class="blog-wrap grid-x grid-margin-x">
	 				<div class="box-info block blog cell small-12">
	 					<h3 class="title">BLOG</h3>
	 					<span class="line line-branco"></span>
	 					<article class="grid-x grid-margin-x">
	 						<div class="cell small-6">
	 							<div class="text">
	 								<p>Já deu para ter uma noção que sabemos tuuuudo sobre formandas né? Você ainda pode conferir nossas dicas no nosso Blog.</p>
	 							</div>
	 						</div>
	 					</article>
	 				</div>
	 			</div>
	 		</div>

	 		<div class="blog-carousel owl-carousel owl-theme">
	 			<div class="blog-carousel-item">
	 				<header style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/diadanoiva.jpg')" class="thumbnail"></header>
	 				<article class="content">
	 						<span class="date">23 DE ABRIL DE 2018</span>
	 						<p class="resume">Mês das noivas está chegando: inspire-se!</p>
	 				</article>
	 			</div>
	 			<div class="blog-carousel-item">
	 				<header style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/diadanoiva.jpg')" class="thumbnail"></header>
	 				<article class="content">
	 						<span class="date">23 DE ABRIL DE 2018</span>
	 						<p class="resume">Mês das noivas está chegando: inspire-se!</p>
	 				</article>
	 			</div>
	 			<div class="blog-carousel-item">
	 				<header style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/diadanoiva.jpg')" class="thumbnail"></header>
	 				<article class="content">
	 						<span class="date">23 DE ABRIL DE 2018</span>
	 						<p class="resume">Mês das noivas está chegando: inspire-se!</p>
	 				</article>
	 			</div>
	 			<div class="blog-carousel-item">
	 				<header style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/diadanoiva.jpg')" class="thumbnail"></header>
	 				<article class="content">
	 						<span class="date">23 DE ABRIL DE 2018</span>
	 						<p class="resume">Mês das noivas está chegando: inspire-se!</p>
	 				</article>
	 			</div>
	 			<div class="blog-carousel-item">
	 				<header style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/diadanoiva.jpg')" class="thumbnail"></header>
	 				<article class="content">
	 						<span class="date">23 DE ABRIL DE 2018</span>
	 						<p class="resume">Mês das noivas está chegando: inspire-se!</p>
	 				</article>
	 			</div>
	 			<div class="blog-carousel-item">
	 				<header style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/diadanoiva.jpg')" class="thumbnail"></header>
	 				<article class="content">
	 						<span class="date">23 DE ABRIL DE 2018</span>
	 						<p class="resume">Mês das noivas está chegando: inspire-se!</p>
	 				</article>
	 			</div>
	 			<div class="blog-carousel-item">
	 				<header style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/diadanoiva.jpg')" class="thumbnail"></header>
	 				<article class="content">
	 						<span class="date">23 DE ABRIL DE 2018</span>
	 						<p class="resume">Mês das noivas está chegando: inspire-se!</p>
	 				</article>
	 			</div>
	 		</div>
		</div>
	</div>

</section>
