<?php
 /*
   Ténicas de Megahair Torriton
 */
?>

<section id="tecnicas" class="wrap-frost-spacing-hair">
  <div class="grid-container full wrap-block-side transparent megahair">
    <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/megahair/tecnicas-mega.jpg')" class="foto-bg"></div>

    <div class="grid-x grid-margin-x align-center block-content">
      <h1 data-aos="fade-right" class="title-big">
        TÉCNICAS DE MEGA HAIR
      </h1>

      <article class="box-info-int box-info-int-white cell small-6">
        <span class="line"></span>
          <div class="text">
          <p>Agora que você decidiu fazer o Mega Hair, começam a surgir várias dúvidas, e como somos especialistas sabemos que você está maluuuca e indecisa para escolher qual das técnicas de Mega Hair vai usar nas suas madeixas!</p>
          </div>
      </article>
    </div>
  </div>
</section>
