<?php
 /*
   Dúvidas
 */
?>
<div id="duvidas" class="grid-container full full-height wrap-block-side">
  <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/megahair/duvidas.jpg')" class="foto-bg full-height"></div>
  <div class="grid-container wrap-block-side transparent full-height">
    <div class="grid-x grid-margin-x align-right block-content">
      <div class="small-9">
        <h1 data-aos="fade-right" class="small-10 cell title-big spacing-bottom title-duvidas title-antes-depois">
          Dúvidas
        </h1>

        <div class="grid-x grid-padding-x box-info-duvidasalign-justify">
          <div class="box-info-int box-info-dica cell small-7">
              <ul class="accordion accordion-duvidas" data-accordion>

          <?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'duvidas-mega-hair' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

              <?php

                for ($i = 1; $i<=5; ++$i) {
                  if( get_field('duvida_' .$i) ): ?>
                        <li class="accordion-item is-active" data-accordion-item>
                          <a href="#" class="accordion-title">
                            <?php the_field("duvida_".$i)?></a>
                          <div class="accordion-content" data-tab-content>
                            <div class="text">
                              <p><?php the_field("resposta_".$i)?>
                               </p>
                            </div>
                            <?php if( get_field('botao_' .$i) ): ?>
                              <div class="btn-area ">
                                <a class="button button--orange" href="<?php the_field("url_btn_".$i); ?>"><?php the_field("botao_" .$i); ?>
                                 <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
                              </div>
                            <?php endif ?>
                          </div>
                        </li>
                    <?php endif; ?>
              <?php } ?>

            <?php endforeach; 
              // Fim do Laço
              wp_reset_postdata();?>

              </ul>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
