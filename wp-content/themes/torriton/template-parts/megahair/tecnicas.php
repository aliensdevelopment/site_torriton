<?php
  /*
    Técnicas
  */
?>

 <section class="itens-wrap servicos-megahair grid-container">
    <div class="grid-x grid-margin-x align-right">
    <div class="small-9">
      <h3 class="t-info-title int">TÉCNICAS</h3>
      <div class="grid-x itens-wrap-int align-justify">

        <?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'tecnicas-mega-hair' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                <article data-aos="fade-up" data-aos-offset="250" data-aos-duration="1000" class="item-block cell noivas">
                    <header class="item-block-title megahair"><?php the_field("titulo_tecnica"); ?></header>

                    <?php 
                        $image = get_field('foto_tecnica');

                        if( !empty($image) ): ?>

                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="item-block-foto" />

                        <?php endif; ?>

                    <div class="item-block-text">
                        <?php the_field("descricao_tecnica"); ?>
                    </div>


                  <!-- Accordion -->
                   <ul class="accordion accordion-servicos" data-accordion>
                    <?php if( get_field('duvida_1') ): ?>
                        <li class="accordion-item is-active" data-accordion-item>
                          <a href="#" class="accordion-title">
                            <?php the_field("duvida_1")?></a>
                          <div class="accordion-content" data-tab-content>
                            <div class="text">
                              <p><?php the_field("resposta_1")?>
                               </p>
                            </div>
                          </div>
                        </li>
                    <?php endif; ?>

                    <?php if( get_field('duvida_2') ): ?>
                        <li class="accordion-item " data-accordion-item>
                          <a href="#" class="accordion-title">
                            <?php the_field("duvida_2")?></a>
                          <div class="accordion-content" data-tab-content>
                            <div class="text">
                              <p><?php the_field("resposta_2")?>
                               </p>
                            </div>
                          </div>
                        </li>
                    <?php endif; ?>

                    <?php if( get_field('duvida_3') ): ?>
                        <li class="accordion-item " data-accordion-item>
                          <a href="#" class="accordion-title">
                            <?php the_field("duvida_3")?></a>
                          <div class="accordion-content" data-tab-content>
                            <div class="text">
                              <p><?php the_field("resposta_3")?>
                               </p>
                            </div>
                          </div>
                        </li>
                    <?php endif; ?>

                    <?php if( get_field('duvida_4') ): ?>
                        <li class="accordion-item " data-accordion-item>
                          <a href="#" class="accordion-title">
                            <?php the_field("duvida_4")?></a>
                          <div class="accordion-content" data-tab-content>
                            <div class="text">
                              <p><?php the_field("resposta_4")?>
                               </p>
                            </div>
                          </div>
                        </li>
                    <?php endif; ?>
                  </ul>

                  <?php if( get_field('botao_principal') ): ?>
                  <div class="btn-area ">
                    <a class="button button--orange" href="<?php the_field("url_btn"); ?>"><?php the_field("botao_principal"); ?>
                     <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
                  </div>
                <?php endif; ?>

                </article>
            <?php endforeach; 
            // Fim do Laço
            wp_reset_postdata();?>

        </div>
    </div>
    </div>
 </section>
