<?php
 /*
    Inspire-se - Antes e Depois
 */
?>

<div class="grid-container wrap-block-side full-height">
  <div class="grid-x grid-margin-x align-right block-content">

    <div class="small-12 medium-9">
      <h1 data-aos="fade-right" class="small-10 cell title-big title-antes-depois">
        ANTES E DEPOIS
      </h1>

      <div class="grid-x grid-padding-x block-dicas-bg block-content-right full-height">
        <div class="cell small-12 medium-6 box-infosub-int box-inspire">
          <span class="line-branco"></span>
          <div class="text">
            <p>Separamos para vocês diversos exemplos inspiradores de antes e depois do Mega Hair. </p>
          </div>
        </div>

      <div class="cell small-5 box-infosub-int">
          <div class="btn-area ">
            <a class="button-white button--white megahair" href="#consulte">Entre em contato 
             <i class="arrow-icon-white"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
            <br/>
            <a class="button-white button--white megahair" href="#consulte">Consulte os preços
            <i class="arrow-icon-white"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Galeria do instagram TODO: mudar de json pra BD-->
      <section data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">
          <div class="galeria carousel-inspire transparent carrosel owl-carousel owl-theme">
          <!-- Galeria Torriton Nail -->
          <?php
            $str = file_get_contents('https://beta02.aliensdesign.com.br/torriton/wp-content/uploads/ig_json/megahairtorriton.json');
            $myposts = json_decode($str, true); 
            foreach ( $myposts["data"] as $post ): // setup_postdata( $post ); ?>
              <div class="galeria-item">
                <a class="thumbnail" target="_blank" href="<?php echo $post['link']; ?>">
                <img onclick="galeriaModal(this)" src="<?php echo $post['images']['standard_resolution']['url']; ?>"/>
                </a>
              </div>
            <?php endforeach;?>
          </div>
      </section>
