<?php
 /*
   Megahair no Torriton
 */
?>

<section id="mega-torriton" class="wrap-frost-spacing-hair">
  <div class="grid-container full wrap-block-side transparent">
    <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/megahair/mega-torriton.jpg')" class="foto-bg"></div>

    <div class="grid-x grid-margin-x align-center block-content">
      <h1 data-aos="fade-right" class="title-big">
        MEGA HAIR NO TORRITON
      </h1>

      <article class="box-info-int box-info-int-white cell small-12 medium-6">
        <span class="line"></span>
          <div class="text">
          <p>Um salão renomado. Profissionais experientes. Técnicas de Mega Hair inovadoras.
            Só poderia ter um resultado: Cliente MEGA satisfeita!</p>
          <p>
            O Torriton Beauty e Hair é pioneiro em Mega Hair em Curitiba, trazendo o luxo, excelência e sofisticação em tudo que fazemos.
            Se o seu sonho é cabelos lindos e volumosos dignos de capa de revista. Aqui é o lugar confie em quem tem anos de experiência em Mega Hair em Curitiba, solicite um orçamento e tire todas as dúvidas.
          </p>
          </div>

          <div class="btn-area ">
            <a class="button button--orange" href="#consulte">Entre em contato 
             <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
            <br/>
            <a class="button button--orange" href="consulte">Consulte os preços
            <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
          </div>
      </article>
    </div>
  </div>
</section>
