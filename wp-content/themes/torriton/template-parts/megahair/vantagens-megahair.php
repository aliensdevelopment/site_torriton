<?php
 /*
   Vantagens Megahair
 */
?>

<div id="vantagens" class="parallax-b cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/megahair/vantagens-mega.jpg"></div>
  <div class="wrap-info-fullhero c grid-container wrap-block-side full-height">
    <div class="grid-x grid-margin-x align-right block-content">
      <div class="small-9">
        <h1 data-aos="fade-right" class="small-7 branco cell title-big">
          VANTAGENS DO MEGA HAIR
        </h1>

        <div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">

          <div class="cell small-12 medium-6 box-infosub-int blocks-itens padding-reset">
            <article>
              <i class="mini-icon"><?php get_template_part('dist/assets/images/inline', 'relogio.svg'); ?></i>
              <div class="text strong-title">
                <p><strong>Prático :</strong> A aplicação de Mega Hair é super simples, demora em média 30 minutos (dependendo da técnica de mega hair escolhida).</p>
              </div>
            </article>
          </div>

          <div class="cell small-12 medium-6 box-infosub-int blocks-itens padding-reset">
            <article>
              <i class="mini-icon"><?php get_template_part('dist/assets/images/inline', 'cloud.svg'); ?></i>
              <div class="text strong-title">
                <p><strong>Volume dos sonhos :</strong> Cabelos com mega hair apresentam mais volume e densidade e principalmente aquele efeito de pontas mais cheias. Além disso os cabelos com espessura mais grossa tendem a embaraçar menos.</p>
              </div>
            </article>
          </div>

          <div class="cell small-12 medium-6 box-infosub-int blocks-itens padding-reset">
            <article>
              <i class="mini-icon"><?php get_template_part('dist/assets/images/inline', 'olho.svg'); ?></i>
              <div class="text strong-title">
                <p><strong>Aspecto Natural :</strong> Utilizamos cabelos naturais e humanos. Trazendo mais vitalidade. Além disso se mescla facilmente com o seu próprio cabelo.</p>
              </div>
            </article>
          </div>

          <div class="cell small-12 medium-6 box-infosub-int blocks-itens padding-reset">
            <article>
              <i class="mini-icon"><?php get_template_part('dist/assets/images/inline', 'smile.svg'); ?></i>
              <div class="text strong-title">
                <p><strong>Indicados para mulheres que sempre sonharam com um cabelão!</strong> Seja porque sofrem problemas de crescimento capilar, passaram por algum corte químico ou até mesmo se arrependeram de cortar e não querem esperar um tempão até o cabelo crescer naturalmente!</p>
              </div>
            </article>
          </div>
   
          <div class="cell small-12 medium-6 box-infosub-int blocks-itens padding-reset">
            <article>
              <div class="text strong-title">
                <p class="destaque-text">Ainda não está convencida ? Agende agora mesmo uma consultoria gratuita com nosso atendimento exclusivo de Mega Hair Curitiba</p>
              </div>
            </article>
          </div>

           <div class="cell small-12 medium-6 box-infosub-int blocks-itens padding-reset">
            <article>
              <div class="btn-area">
              <a class="button button--orange" href="#consulte"> Entre em contato
              <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
              </div>
               <div class="btn-area">
              <a class="button button--orange" href="#consulte"> Consulte os preços
              <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
              </div>
            </article>
          </div>
 
        </div>
      </div>
    </div>
  </div>
