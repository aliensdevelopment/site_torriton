<?php // Serviços Nails ?>

<div class="parallax-b cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/nails/servicos.jpg"></div>
<div id="servicos" class="wrap-info-fullhero e grid-container wrap-block-side  full-height">
  <div class="grid-x grid-margin-x align-right block-content servicos-nails">
    <div class="small-9 ">
      <h1 class="t-title-diamond small-5 cell branco">
        SERVIÇOS
      </h1>

      <div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
        <div class="cell small-12 box-infosub-int">
          <div class="text">
            <p>Confira todos os serviços oferecidos no Torriton Nail.</p>
          </div>
          <h2 class="title t-info-title">Manicure | Pedicure | Unhas decoradas | Unhas com pedras | Unhas postiças
            Unha de porcelana | Spa dos pés | Spa das mãos | Podologia</h2>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="itens-wrap servicos nails-wrap grid-container">
<div class="grid-x grid-margin-x align-right">
 <div class="small-9">
   <h3 class="t-info-title int">ALONGAMENTO DE UNHAS</h3>
   <div class="grid-x itens-wrap-int align-justify">

      <?php
      global $post;
      $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'servicos-torriton-nail' );

      $myposts = get_posts( $args );

      // Início do Laço selecionado pelo nome da categoria
      foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
          <article data-aos="fade-up" data-aos-offset="250" data-aos-duration="1000" class="item-block cell ">
              <header class="item-block-title purple"><?php the_field("titulo_servico_unha"); ?></header>

              <?php 
                  $image = get_field('foto_destaque_unha');

                  if( !empty($image) ): ?>

                      <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="item-block-foto" />

                  <?php endif; ?>

              <div class="item-block-text">
                  <p><?php the_field("descricao_servico_unha"); ?></p>
              </div>

          </article>
      <?php endforeach; 
      // Fim do Laço
      wp_reset_postdata();?>

    </div>
 </div>
</div>
</section>