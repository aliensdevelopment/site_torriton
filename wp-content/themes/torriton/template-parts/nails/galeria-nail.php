<?php // Inspire-se Nail ?>

<div id="inspire-se" class="grid-container wrap-block-side full-height">
<div class="grid-x grid-margin-x align-right block-content">
  <div class="small-9">
    <h1  data-aos="fade-right" class="small-10 cell t-title-diamond title-insp">
      INSPIRE-SE
    </h1>

    <div class="grid-x grid-padding-x block-dicas-inspire block-content-right full-height">
      <div class="cell small-4 box-infosub-int">
        <span class="line line-branco"></span>
        <div class="text confira">
          <p>Confira aqui todos os trabalhos realizados por nossas manicures no espaço Torriton Nail. </p>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<!-- Galeria do instagram TODO: mudar de json pra BD-->
<section data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">
  <div class="galeria carousel-inspire transparent carrosel owl-carousel owl-theme">
  <!-- Galeria Torriton Nail -->
  <?php
    $str = file_get_contents('https://beta02.aliensdesign.com.br/torriton/wp-content/uploads/ig_json/torritonnail.json');
    $myposts = json_decode($str, true); 
    foreach ( $myposts["data"] as $post ): // setup_postdata( $post ); ?>
      <div class="galeria-item">
        <a class="thumbnail" target="_blank" href="<?php echo $post['link']; ?>">
        <img onclick="galeriaModal(this)" src="<?php echo $post['images']['standard_resolution']['url']; ?>"/>
        </a>
      </div>
    <?php endforeach;?>
  </div>
</section>