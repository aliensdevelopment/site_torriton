<?php
 /*
   Hotel da Noiva
 */
?>

<div class="parallax-b cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/hero-hotel.jpg"></div>
<!-- <div class="frost-wrapper"></div> -->
  <div class="wrap-info-fullhero c grid-container wrap-block-side full-height">
    <div class="grid-x grid-margin-x align-right block-content">
      <div class="small-12 medium-9">
        <h1 data-aos="fade-right" class="small-5 branco cell title-big">
          HOTEL DIA DA NOIVA
        </h1>

        <div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
          <div class="box-info-int full-width box-info-dica cell small-12 medium-12">
            <span class="line"></span>
            <h2 class="title">Dica 8:</h2>
            <div class="text dica">
              <p>"Quer mesmo o hotel ou serviço a domicilio? "</p>
            </div>
          </div>

          <div class="cell small-12 medium-10 box-infosub-int padding-reset">
            <article>
              <h2 class="title">Serviço em Hotel ou no domicilio! Saiba a verdade!</h2>
              <div class="text">
                <p>Atendemos ao desejo dos clientes até no hotel, mas a modo nosso e a garantia da cliente final!</p>
              </div>
            </article>
          </div>

          <div class="cell small-12 medium-6 box-infosub-int blocks-itens padding-reset">
            <article>
              <i class="mini-icon"><?php get_template_part('dist/assets/images/inline', 'smile.svg'); ?></i>
              <div class="text strong-title">
                <p><strong>Economia!</strong> Garantimos um preço justo graças a acordos prévios com os profissionais que sabemos ter as competências para lhe atender com qualidade e responsabilidade. Mesmo assim não podemos lhe garantir o preço do salão pelos motivos da dica 8;</p>
              </div>
            </article>
          </div>

          <div class="cell small-12 medium-6 box-infosub-int blocks-itens padding-reset">
            <article>
              <i class="mini-icon"><?php get_template_part('dist/assets/images/inline', 'contrato.svg'); ?></i>
              <div class="text strong-title">
                <p><strong>Contrato!</strong> Um contrato escrito em que detalhamos e assumimos claramente as responsabilidades para tranquilidade que o seu dia da noiva merece. Incluindo alternativas no caso de irresponsabilidade de um profissional no seu dia da noiva;</p>
              </div>
            </article>
          </div>

          <div class="cell small-12 medium-6 box-infosub-int blocks-itens padding-reset">
            <article>
              <i class="mini-icon"><?php get_template_part('dist/assets/images/inline', 'tesoura.svg'); ?></i>
              <div class="text strong-title">
                <p><strong>Desconto!</strong> Repassamos um desconto significativo sobre as suítes para noivas no nosso parceiro Radisson Hotel que nos garante a melhor estrutura, amplo espaço para receber as equipes e convidadas, design de interiores e serviços complementares dignos de um dia da noiva elegante e sem surpresas;</p>
              </div>
            </article>
          </div>

          <div class="cell small-12 medium-6 box-infosub-int blocks-itens padding-reset">
            <article>
              <i class="mini-icon"><?php get_template_part('dist/assets/images/inline', 'chat.svg'); ?></i>
              <div class="text strong-title">
                <p><strong>Flexibilidade!</strong> Já fechou com outro hotel? Ok! Vamos trabalhar juntos com o hotel para garantir que tenha as características e os serviços essenciais para um atendimento de qualidade;</p>
              </div>
             <div class="btn-area">
              <a class="button button--noivas" href="#consulte">Entre em contato
                      <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>

              <a class="button button--noivas" href="#consulte">Consulte os preços
                      <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
            </div>
            </article>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Implementar -->
<section data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">
   <div class="grid-container">
      <div class="galeria-wrap grid-x grid-margin-x align-right">
        <div class="small-9">
          <div class="box-info size-m cell">
            <span class="line"></span>
            <h3 class="title">HOTEL RADISSON</h3>
          </div>
        </div>
      </div>
    </div>
    <div class="galeria carrosel owl-carousel owl-theme ">
        <?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-hotel-radisson' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                <?php 

                $images = get_field('galeria');

                if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                            <div class="galeria-item">
                              <img onclick="galeriaModal(this)" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />
                             </div>
                        <?php endforeach; ?>
                <?php endif; ?>

            <?php endforeach; 
            // Fim do Laço
          wp_reset_postdata();?>
    </div>
</section> 
<?php get_template_part( 'template-parts/galerias/galeria-foto' ); ?> 

<section id="seusconvidados" class="wrap-color-full">
  <div class="grid-container wrap-block-side transparent padding-reset full-height">
    <div class="grid-x grid-margin-x align-right block-content">
      <div class="small-12 medium-12 large-9">
        <div class="grid-x grid-padding-x block-dicas-bg align-justify">
          <div class="box-info-int box-info-dica cell small-12 medium-6">
           <h1 data-aos="fade-right" class="small-12 medium-6 branco cell title-big title-dicas">
            Seus convidados
          </h1>
            <span class="line line-branco"></span>
            <h2 class="title">Dica 9:</h2>
            <div class="text dica">
              <p>“ Você precisa informar e gerenciar de forma rápida os padrinhos e convidados! ”</p>
              </div>
          </div>

          <div class="cell small-12 medium-6  box-infosub-int">
            <article class="small-box">
              <h2 class="title">Gerenciamos os seus convidados em pouquíssimos cliques! Criamos uma plataforma digital exclusiva porque a nossa experiência fala mais alto e sabemos que vai precisar disso! Te ajuda e nos ajuda! Muito!</h2>
              <div class="text">
                <p>Sabia que as madrinhas e convidadas podem atrasar o seu casamento? Sabia que convidados de fora de Curitiba precisam sempre de uma referência de salão sem surpresa no preço e na qualidade do serviço? Sabia que podemos oferecer descontos ao grupo inteiro de convidados? 
                E com este intuito que planejamos e lançamos uma inovativa e exclusiva plataforma própria de gerenciamento de padrinhos e convidados. Informar, agendar, monitorar todos os convidados que decidem se produzir no Torriton será extremamente rápido, simples e conveniente! Deixe isto com a gente, isto é trabalho nosso!
                Adivinha? É incluído! 
                E não termina por aqui! A partir da confirmação de um certo número de convidados, os mesmos até recebem descontos individuais e preços garantidos! Agende uma consultoria gratuita para o seu dia da noiva no Torriton.</p>
              </div>
              <div class="btn-area">
              <a class="button button-white noivas" href="#consulte">Entre em contato
                      <i class="arrow-icon-white"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>

              <a class="button button-white noivas" href="#consulte">Consulte os preços
                      <i class="arrow-icon-white"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
            </div>
            </article>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_template_part( 'template-parts/forms/formulario' ); ?> 