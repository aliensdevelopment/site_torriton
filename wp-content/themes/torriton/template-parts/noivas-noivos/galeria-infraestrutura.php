<?php
 /*
    Galeria Infraestrutura
 */
?>

<div class="grid-container wrap-block-side top-margin full-height">
  <div class="grid-x grid-margin-x align-right block-content">

    <div class="small-12 medium-9">
      <div class="grid-x grid-padding-x block-dicas-nobg">
        <div class="box-info-int box-info-dica cell small-12 medium-7">
          <span class="line"></span>
          <h2 class="title">Dica 5:</h2>
          <div class="text dica">
            <p>“Dê preferência a quem precisa de uma atenção especial ”
            </p>
            </div>
        </div>

        <div class="cell small-12 medium-5 box-infosub-int">
          <article class="small-box">
            <h2 class="title">Necessidades especiais!
                Você precisa e eles também!</h2>
            <div class="text">
              <p>Idosos, cadeirantes, crianças... Além de ser profundamente justo cuidar de quem tem necessidades especiais é prioridade estratégica no planejamento do seu dia da noiva. Temos a disposição uma rampa de entrada, cadeira de roda, banheiro para deficiente, piso central sem degraus e trocador de fraudas. Converse com a nossa consultoria gratuita para o dia da noiva Torriton e resolva antes que o seu dia vire um estresse desnecessário. </p>
            </div>
            <div class="btn-area">
              <a class="button button--noivas" href="#consulte">Entre em contato
                      <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>

              <a class="button button--noivas" href="#consulte">Consulte os preços
                      <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
            </div>
          </article>
        </div>
      </div>
    </div>
  </div>
</div>