<?php
 /*
   Infraestrutura
 */
?>

<div class="grid-container full wrap-block-side full-height">
  <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/infraestrutura.jpg')" class="foto-bg top-align"></div>
    <div class="grid-container wrap-block-side full-height-padding">
      <div class="grid-x grid-margin-x align-right block-content">

        <div class="small-12 medium-9">
          <h1 data-aos="fade-right" class="small-10 cell title-big spacing-bottom-reset title-pacote-noivas">
            INFRAESTRUTURA
          </h1>

          <div class="grid-x grid-padding-x block-dicas-nobg align-justify">
            <div class="box-info-int box-info-dica cell small-12 medium-6">
              <span class="line"></span>
              <h2 class="title">Dica 4:</h2>
              <div class="text dica">
                <p>“Evite salões que não lhe
                  podem garantir uma
                  boa logística! ”
                </p>
                </div>
            </div>

            <div class="cell small-12 medium-5 box-infosub-int">
              <article class="small-box">
                <h2 class="title">Estacionamento e logística!<br/> Nós temos!</h2>
                <div class="text">
                  <p>Noivos, madrinhas e padrinhos, convidadas e convidados do seu casamento... todos vão ter estacionamento gratuito e serviço de manobrista incluso. Se tem convidados de fora temos área de embarque e desembarque para eventuais serviços de transporte e afins. Dia da Noiva com madrinhas, família, e todas as convidadas, aqui cabe todo mundo!</p>
                </div>
              <div class="btn-area">
                <a class="button button--noivas" href="#consulte">Entre em contato
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>

                <a class="button button--noivas" href="#consulte">Consulte os preços
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
              </div>
              </article>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
