<?php
 /*
  Hero Vídeo
 */
?>

<div class="parallax-window noivas-video-wrap cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/noiva-videobg.jpg">
  <div class="noivas-video-content grid-container full">
    <div class="grid-x grid-margin-x align-middle align-center">
      <a class="cell icon-play" href="#"><?php get_template_part('dist/assets/images/inline', 'play.svg'); ?>
      </a>
    </div>
  </div>
  <div class="text-video">
  <p class="medium-6 medium-offset-4">Noivas e Noivos Torriton</p>
 </div>
</div>
