<?php
 /*
  Hero
 */
?>

<div class="grid-container full wrap-block-side full-height wrap-header-spacing wrap-padding-reset">

  <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/noivo.jpg')" class="foto-bg bg-left"></div>

  <div class="grid-container wrap-block-side transparent full-height padding-b-reset">
    <div class="grid-x grid-margin-x align-right block-content">
      <div class="small-9">
        <div class="grid-x align-right">
          <h1 data-aos="fade-right" class="small-5 cell title-big">
            Dia do Noivo
          </h1>
        </div>

        <div class="grid-x grid-padding-x block-dicas-bgwhite top-spacing align-justify">
          <div class="box-info-int box-info-dica cell small-6">
            <span class="line"></span>
            <h2 class="title">Dica 10:</h2>
            <div class="text dica">
              <p> “O noivo deixa tudo para o último minuto do tempo suplementar.” </p>
              </div>
          </div>

          <div class="cell small-5 box-infosub-int">
            <article class="small-box">
              <h2 class="title">Lembre ao noivo que ele também vai casar! Você precisa ajuda-lo! </h2>
              <div class="text">
                <p>Não é culpa dele, não o faz de proposito e sim, parece que não, mas ele te ama! Quer ajuda-lo? Venha com ele junto no dia da consulta gratuita do Dia da Noiva em Curitiba e temos todas as dicas e soluções para o dia do noivo também!</p>
              </div>
              <div class="btn-area">
                <a class="button button--noivas" href="#consulte">Entre em contato
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>

                <a class="button button--noivas" href="#consulte">Consulte os preços
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
              </div>
            </article>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
