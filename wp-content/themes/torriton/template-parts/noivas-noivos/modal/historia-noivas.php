<?php
// Modal História das Noivas
// Separado por categoria - galeria-noiva-historia
?>

<?php // Item Modal 1?>
  <div class="reveal" id="modalNoivas" data-reveal >
    <button class="close-button" data-close aria-label="Close modal" type="button">
      <span aria-hidden="true">&times;</span>
    </button>

    <div class="grid-container">
     <div class="grid-x grid-padding-x block-content">
        <div class="medium-12">
          <?php

            global $post;
            $args = array( 'posts_per_page' => 1, 'offset'=> 0, 'category_name' => 'galeria-noiva-historia-1' );

            $myposts = get_posts( $args );

            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

            <h2 class="t-title-diamond title-promocoes">
              <?php the_field('titulo_historia');?></h2>
              <!-- Verifica imagem -->
                  <?php 
                      $image = get_field('foto_noiva');

                      if( !empty($image) ): ?>

                          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="foto-modal" />

                      <?php endif; ?>
              <div>
                <p class="title-modal"><?php the_field('texto_historia');?></p>
              </div>
              <div>
                <p class="conteudo-modal"><?php the_field('conteudo_historia');?></p>
              </div>

              <div class="grid-container">
                <div class="galeria-wrap grid-x grid-margin-x align-right">
                  <div class="small-12 medium-12">
                    <div class="box-info noivas box-modal cell">
                    <span class="line line-box"></span>
                    <h3 class="title tittle-box-noivas">GALERIA</h3>
                    </div>
                  </div>
                </div>
                <div class="galeria carrosel galeria-noivas galeria-modal owl-carousel owl-theme ">
                <?php //Foto Galeria ?>
                  <?php if( get_field('foto_galeria_modal_1') ): ?>
                    <div class="galeria-item item-noiva">
                        <?php 
                            $image = get_field('foto_galeria_modal_1');

                            if( !empty($image) ): ?>

                                <img onclick="galeriaModal(this)" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />

                                  <?php endif; ?>
                      </div>
                    <?php endif; ?>
                  <?php //Fim Foto Galeria ?>

                <?php //Foto Galeria ?>
                  <?php if( get_field('foto_galeria_modal_2') ): ?>
                    <div class="galeria-item item-noiva">
                        <?php 
                            $image = get_field('foto_galeria_modal_2');

                            if( !empty($image) ): ?>

                                <img onclick="galeriaModal(this)" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />

                                  <?php endif; ?>
                      </div>
                    <?php endif; ?>
                  <?php //Fim Foto Galeria ?>

                <?php //Foto Galeria ?>
                  <?php if( get_field('foto_galeria_modal_3') ): ?>
                    <div class="galeria-item item-noiva">
                        <?php 
                            $image = get_field('foto_galeria_modal_3');

                            if( !empty($image) ): ?>

                                <img onclick="galeriaModal(this)" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />

                                  <?php endif; ?>
                      </div>
                    <?php endif; ?>
                  <?php //Fim Foto Galeria ?>

                <?php //Foto Galeria ?>
                  <?php if( get_field('foto_galeria_modal_4') ): ?>
                    <div class="galeria-item item-noiva">
                        <?php 
                            $image = get_field('foto_galeria_modal_4');

                            if( !empty($image) ): ?>

                                <img onclick="galeriaModal(this)" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />

                                  <?php endif; ?>
                      </div>
                    <?php endif; ?>
                  <?php //Fim Foto Galeria ?>

                <?php //Foto Galeria ?>
                  <?php if( get_field('foto_galeria_modal_5') ): ?>
                    <div class="galeria-item item-noiva">
                        <?php 
                            $image = get_field('foto_galeria_modal_5');

                            if( !empty($image) ): ?>

                                <img onclick="galeriaModal(this)" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />

                                  <?php endif; ?>
                      </div>
                    <?php endif; ?>
                  <?php //Fim Foto Galeria ?>
                </div>
              </div>

          <?php endforeach; 
          wp_reset_postdata();?>

        </div>
     </div>
    </div>
    <?php get_template_part( 'template-parts/galerias/galeria-foto' ); ?>
  </div>
<?php // Item Modal ?>
