<?php
 /*
   Sala da Noiva
 */
?>

<div id="sala-noiva" class="grid-container full wrap-block-side full-height wrap-padding-reset">

  <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/sala-noiva.jpg')" class="foto-bg bg-sala-noiva"></div>

  <div class="grid-container wrap-block-side transparent full-height padding-b-reset">
    <div class="grid-x grid-margin-x align-right block-content">
      <div class="small-12 medium-9">
        <h1 data-aos="fade-right" class="small-5 cell title-big">
          SALA DA NOIVA
        </h1>

        <div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
          <div class="box-info-int box-info-dica cell small-12 medium-6">
            <span class="line"></span>
            <h2 class="title">Dica 7:</h2>
            <div class="text dica">
              <p>“Privacidade e atenção dependem de estrutura física, pessoas experientes e suporte!”
              </p>
              </div>
          </div>

          <div class="cell small-12 medium-5 box-infosub-int">
            <article class="small-box">
              <h2 class="title">Atenção exclusiva e privacidade no salão! Você precisa disto!</h2>
              <div class="text">
                <p>Serviços em casa ou no hotel nunca poderão lhe garantir a presença e a pontualidade dos profissionais contratados verbalmente. Também não garantem substitutos em caso de imprevistos e principalmente não contam com uma estrutura de equipamentos que você precisa. 
                As salas privativas do Torriton são dedicadas e previamente equipadas com espelhos, cadeiras profissionais, cabides e tudo o que você precisa para garantir sua privacidade. Além disso você pode contar com o experiente atendimento da gerência de um salão para resolver qualquer situação ou mudança do último minuto. </p>
                <p>E o custo benefício? O preço no hotel de um profissional diretamente contratado é de até algumas vezes maior do que nos lhe podemos garantir no salão! 
                Um profissional deixa de ganhar com as suas clientes habituais e deve compensar para ganhar somente com você. Ruim para o profissional porque a clientela habitual deixa de ser atendida, e perde de ganhar no dia por causa do volume de atendimentos perdidos. É também ruim para você, pela garantia do serviço! Qualquer inconveniente não tem como ter substitutos e nem tem contrato para lhe garantir isso, além de ser muito ruim para o seu bolso!</p>
                <p>Quer privacidade no salão? Temos salas privativas equipadas exclusivamente para atender as noivas e noivos, e o espaço é amplo para seu melhor conforto e conforto de seus convidados.</p>
              </div>
               <div class="btn-area">
                <a class="button button--noivas" href="#consulte">Entre em contato
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>

                <a class="button button--noivas" href="#consulte">Consulte os preços
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
              </div>
            </article>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
