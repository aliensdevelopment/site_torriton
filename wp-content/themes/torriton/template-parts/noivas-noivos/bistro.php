<?php
 /*
   Bistrô
 */
?>

<div class="parallax-b cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/hero-food.jpg"></div>
<!-- <div class="frost-wrapper"></div> -->
<div class="frost-wrap">
  <div class="wrap-info-fullhero b grid-container wrap-block-side full-height padding-b-reset">
    <div class="grid-x grid-margin-x align-right block-content">
      <div class="small-12 medium-9">
        <div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
          <div class="box-info-int box-info-dica cell small-12 medium-6">
            <span class="line"></span>
            <h2 class="title">Dica 6:</h2>
            <div class="text dica">
              <p>“ O tempo médio de permanência de uma noiva é de três horas. ”</p>
              </div>
          </div>

          <div class="cell small-12 medium-5 box-infosub-int">
            <article class="small-box">
              <h2 class="title">Precisa beber, comer e relaxar! Não desmaie antes de dizer “sim, eu aceito!”</h2>
              <div class="text">
                <p>Além de ser uma necessidade, comidas e bebidas precisam ser de qualidade! O nosso Torriton Bistrot é um verdadeiro restaurante comandado por uma chef qualificada e uma equipe experiente. Não tem nenhum salão que tenha este nível de estrutura dedicada ao “bem estar do estomago! ”. Com amplo e elegante espaço interno e externo coberto. Quer organizar uma recepção privativa para convidados? Um esquenta para madrinhas e padrinhos? Seja bem-vindo! Aqui vai dar certo! A nossa consultoria gratuita para o Dia da Noiva em Curitiba pode-lhe mostrar o que é preciso e você vai descobrir que é muito mais em conta do que está imaginando! </p>
              </div>
              <a href="https://beta02.aliensdesign.com.br/torriton/torriton-bistrot/" class="dest">Confira mais detalhes do Torriton Bistrot Aqui!!</a> 
               <div class="btn-area">
                <a class="button button--noivas" href="#consulte">Entre em contato
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>

                <a class="button button--noivas" href="#consulte">Consulte os preços
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
              </div>
            </article>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
