<?php
 /*
    Galeria Sala das Noivas
 */
?>

<section data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">
	 <div class="grid-container">
			<div class="galeria-wrap grid-x grid-margin-x align-right">
        <div class="small-9">
          <div class="box-info noivas cell">
  					<span class="line line-box"></span>
  					<h3 class="title tittle-box-noivas">FOTOS SALA DA NOIVA</h3>
  				</div>
        </div>
			</div>
		</div>

		<div class="galeria carrosel owl-carousel owl-theme ">
		        <?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-sala-noivas' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                <?php 

                $images = get_field('galeria');

                if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                            <div class="galeria-item">
                              <img onclick="galeriaModal(this)" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />
                             </div>
                        <?php endforeach; ?>
                <?php endif; ?>

            <?php endforeach; 
            // Fim do Laço
          wp_reset_postdata();?>
		</div>
</section>
    <!-- Galeria -->
  <?php get_template_part( 'template-parts/galerias/galeria-foto' ); ?>