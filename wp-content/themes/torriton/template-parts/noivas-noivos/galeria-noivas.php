<?php
 /*
    Galeria Noivas
 */
?>

<section data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">
	 <div class="grid-container">
			<div class="galeria-wrap grid-x grid-margin-x align-right">
        <div class="small-12 medium-9">
            <div class="box-info noivas cell">
            	<p class="text-conheca"><em>Conheça a história das noivas e noivos
            	que já passaram por aqui</em></p>
				<span class="line line-box"></span>
				<h3 class="title tittle-box-noivas">Galeria</h3>
  			</div>
        </div>
			</div>
		</div>

		<div class="galeria carrosel galeria-noivas owl-carousel owl-theme ">

		<!-- data-open="modalNoivas" --> 

        <?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-noivas-torriton' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                <?php 

                $images = get_field('galeria');

                if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                            <div class="galeria-item">
                              <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />
                             </div>
                        <?php endforeach; ?>
                <?php endif; ?>

            <?php endforeach; 
            // Fim do Laço
          wp_reset_postdata();?>
    </div>
</section>

  <section class="cd-section">
    <?php get_template_part( 'template-parts/noivas-noivos/modal/historia-noivas' ); ?>
  </section>
