<?php
 /*
   Intro
 */
?>

<div class="grid-container wrap-block-side full-height">
  <div class="grid-x grid-margin-x align-right block-content">

    <div class="small-9">
      <div class="grid-x grid-padding-x block-dicas-nobg">
        <div class="box-info-int box-info-dica cell small-12 medium-6">
          <span class="line"></span>
          <h2 class="title">Dica 1:</h2>
          <div class="text dica">
            <p>"Confie em quem pode
              provar o que fala”
            </p>
            </div>
        </div>

        <div class="cell small-12 medium-5 box-infosub-int">
          <article class="small-box">
            <h2 class="title">Confiança?! Temos provas!</h2>
            <div class="text">
              <p>Desde 1977, milhares de Noivas e Noivos confiaram na consultoria gratuita do dia da noiva Torriton e nos pacotes personalizados do nosso Dia da Noiva em Curitiba. </p>
            </div>
            <div class="btn-area">
              <a class="button button--noivas" href="#consulte">Entre em contato
                      <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>

              <a class="button button--noivas" href="#consulte">Consulte os preços
                      <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
            </div>
          </article>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_template_part( 'template-parts/noivas-noivos/galeria-noivas' ); ?>

<div class="parallax-b cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/transparencia-noivas.jpg" ></div>

<div class="frost-wrapper"></div>

<div class="wrap-info-fullhero grid-container wrap-block-side full-height">
  <div class="grid-x grid-margin-x align-right block-content">

    <div class="small-12 medium-12 large-9">
      <div class="grid-x grid-padding-x block-dicas-bg align-justify">
        <div class="box-info-int box-info-dica cell small-12 medium-6">
          <h2 class="title">Dica 2:</h2>
          <div class="text dica">
            <p>“Evite o fechamento verbal. Nunca vai dar certo!”
            </p>
            </div>
        </div>

        <div class="cell small-12 medium-6 box-infosub-int">
          <article class="small-box">
            <h2 class="title">Transparência é Preto no Branco!</h2>
            <div class="text">
              <p>Durante a consultoria gratuita do Dia da Noiva, além de conhecer o nosso espaço e também nossos profissionais e funcionários de apoio, você poderá conferir um documento detalhado que serve de referência no caso do fechamento formal do pacote personalizado. Nada é deixado de lado e o seu dia vai ser fantástico como você imaginou.
              Cada casamento e cada planejamento é único e muitos contratempos podem ser evitados conversando e planejando previamente com a nossa experiente consultoria gratuita do Dia da Noiva em Curitiba. Sem compromisso nenhum! 
            </p>
            </div>
            <div class="btn-area">
              <a class="button button-white noivas" href="#consulte">Entre em contato
                      <i class="arrow-icon-white"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>

              <a class="button button-white noivas" href="#consulte">Consulte os preços
                      <i class="arrow-icon-white"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
            </div>
          </article>
        </div>
      </div>
    </div>
  </div>
</div>

<section id="pacotes-personalizados">
  <div class="grid-container full wrap-block-side">
    <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/pacotes-personalizados.jpg')" class="foto-bg"></div>
    <div class="grid-container wrap-block-side transparent full-height">
      <div class="grid-x grid-margin-x align-right block-content">
        <div class="small-12 medium-12 large-9">
          <h1 data-aos="fade-right" class="small-10 cell title-big spacing-bottom title-pacote-noivas">
            PACOTES PERSONALIZADOS
          </h1>

          <div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
            <div class="box-info-int box-info-dica cell small-12 medium-7">
              <span class="line"></span>
              <h2 class="title">Dica 3:</h2>
              <div class="text dica">
                <p>“Evite salões que lhe impõem serviços desnecessários”</p>
                </div>
            </div>

            <div class="cell small-12 medium-4 box-infosub-int">
              <article class="small-box">
                <h2 class="title">Economia! Vai se surpreender com a nossa fórmula aberta e flexível!</h2>
                <div class="text">
                  <p>A nossa consultoria gratuita do Dia da Noiva em Curitiba lhe permite detalhar e formalizar uma descrição do seu dia da noiva e dos serviços realmente oportunos ou desejados para montar um pacote personalizado. 
                  Recebemos muitas perguntas: “Quanto custa o Dia da Noiva?” Não temos como dizer, afinal, você tem total flexibilidade para escolher os seus serviços fazendo assim um dia da noiva perfeito para você!
                  </p>
                </div>
                <div class="btn-area">
                <a class="button button--noivas" href="#consulte">Entre em contato
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>

                <a class="button button--noivas" href="#consulte">Consulte os preços
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
              </div>
              </article>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="bg-consultoria pacotes">
  </div>
</section>