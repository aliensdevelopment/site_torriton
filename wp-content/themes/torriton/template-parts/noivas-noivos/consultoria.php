<?php
 /*
   Consultoria
 */
?>

<div class="grid-container full wrap-block-side">
  <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/consultoria.jpg')" class="foto-bg bg-box-agenda"></div>

  <div class="grid-x grid-margin-x align-center block-content">
    <h1 data-aos="fade-right" class="title-big title-noivas">
      CONSULTORIA GRATUITA

    </h1>

    <article class="box-info-int box-agenda cell small-12 medium-6">
      <span class="line"></span>
      <div class="text">
        <p>
          Agende agora uma consulta gratuita com quem tem 
          4 décadas de experiência e confiança do mercado! Faça isso o quanto antes! Sem nenhum compromisso!</p>
        <p>
        O único favor que lhe pedimos é remarcar ou desmarcar caso não puder vir. Você vai se surpreender com o nosso profissionalismo e transparência.</p>
      </div>
      <div class="btn-area">
        <a class="button button--noivas" href="#consulte">Entre em contato
                <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>

        <a class="button button--noivas" href="#consulte">Consulte os preços
                <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
      </div>
    </article>
  </div>
</div>
