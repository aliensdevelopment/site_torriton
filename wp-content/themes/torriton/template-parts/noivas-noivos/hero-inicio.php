<?php
 /*
  Hero
 */
?>

<header  class="hero-int" role="banner">
	<div data-aos="fade-right" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/noivas-wrap.jpg')" class="foto-bg"></div>
	<section id="sobre" class="conteudo grid-x align-right">
        <h1 data-aos="fade-left" data-aos-duration="1200" class="title-big">
          DIA DA
          NOIVA E DO NOIVO
				</h1>
				<div class="box-info-int box-info-mb cell small-12 medium-6">
					<h2 class="title">SOBRE</h2>
					<div class="text">
						<p><em>O melhor do Dia da Noiva Curitiba, você encontra aqui! </em></p>
						<p>Desde 1977 temos orgulho de levar ao altar o fruto do nosso trabalho junto com as noivas.
							Com tantos anos de experiência, sem dúvidas podemos garantir que o seu dia da noiva será mais do que especial.
						</p>
						<p>
							Somos o Salão de beleza para o seu Dia da Noiva mais completo de Curitiba. E para provar que sabemos do que estamos falando, preparamos 10 dicas que poderá antecipar o planeamento para o seu dia da noiva. Antes de tudo, uma dica é sem dúvida a mais importante de todas: </p>
					</div>
				</div>
	</section>
</header>

<div class="grid-container infosub-wrap box-dica-ouro">
	<div class="grid-x grid-margin-x align-right">
		<div class="cell small-12 medium-7 box-infosub-int">
			<span class="line"></span>
			<div class="text ">
				<p><em>DICA DE OURO: Não importa onde você vai fechar o seu dia da noiva ou dia do noivo, a escolha é sua! Mas para escolher rapidamente e sem se arrepender a vida inteira, tem que saber tudo o que você precisa fazer, tudo o que NÃO deve fazer, tudo o que o mercado de salões e profissionais da beleza lhe oferecem, tudo o que este mercado não lhe fala abertamente, e todos os riscos oportunidades. Tudo isto preferivelmente antes do dia tão sonhado, não é?!</em></p>
			</div>
		</div>
	</div>
</div>
