<?php
 /*
   Torriton Men
 */
?>

<section id="servicos">
  <div class="parallax-c cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/men/torriton-men-02.jpg"></div>
  <div class="wrap-info-fullhero c grid-container wrap-block-side full-height">
    <div class="grid-x grid-margin-x align-right block-content estetico-torriton-centro">
      <div class="small-9">
        <h1 data-aos="fade-right" class="small-5 branco cell t-title-diamond">
		  SERVIÇOS
        </h1>

        <div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
          <div class="box-info-int full-width box-info-dica cell medium-8 small-12">
			 	<p class="t-info-text">
					Além dos mais variados Cortes de cabelo masculino, o Torriton Men é conceito e referência no ramo de barbearia em Curitiba, oferecendo serviços clássicos de cuidados e beleza para o público masculino.  
				</p>
				<p class="t-info-text">
					Oferecemos também todos os serviços estéticos para o homem que presa pela elegância e conforto.
				</p>
				<a href="#" class="item-block-link">Confira aqui todos os serviços oferecidos. <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
