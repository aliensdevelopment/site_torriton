<header class="site-header int" role="banner">
  <!-- <div class="site-title-bar title-bar" <?php foundationpress_title_bar_responsive_toggle(); ?>>
    <div class="title-bar-left">
      <button aria-label="<?php _e( 'Main Menu', 'foundationpress' ); ?>" class="menu-icon" type="button" data-toggle="offCanvasLeft"></button>
      <span class="site-mobile-title title-bar-title">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
      </span>
    </div>
  </div> -->

  <nav class="site-navigation top-bar int grid-container" role="navigation">
    <div class="top-bar-left">
      <div class="site-desktop-title top-bar-logo">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
          <?php get_template_part('dist/assets/images/inline', 'logo.svg'); ?>
        </a>
      </div>
    </div>
    <div class="top-bar-right">
      <?php foundationpress_top_bar_r(); ?>
      <?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) === 'topbar' ) : ?>
        <?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
      <?php endif; ?>
    </div>
    <button type="button" data-toggle="offCanvasRight" class="menu-hamburguer">
      <?php get_template_part('dist/assets/images/inline', 'menu.svg'); ?>
    </button>
    <button class="search-button" data-toggle="pesquisar"><i><?php get_template_part('dist/assets/images/inline', 'search.svg'); ?></i></button>
  </nav>

  <div class="grid-container">
    
    <div class="reveal fast ease-in-out" id="pesquisar" data-reveal data-close-on-click="true" data-animation-in="slide-in-down" data-animation-out="slide-out-up">
      <?php get_template_part( 'template-parts/header-search' ); ?>
    </div>
  </div>
</header>

<?php /* Fixo */ ?>

<header id="header" class="site-header ha-header"  role="banner">
  <div class="site-title-bar title-bar" <?php foundationpress_title_bar_responsive_toggle(); ?>>
    <div class="title-bar-left">
      <button aria-label="<?php _e( 'Main Menu', 'foundationpress' ); ?>" class="menu-icon" type="button" data-toggle="<?php foundationpress_mobile_menu_id(); ?>"></button>
      <span class="site-mobile-title title-bar-title">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
      </span>
    </div>
  </div>

  <nav class="site-navigation top-bar fixed grid-container" role="navigation">
    <div class="top-bar-left">
      <div class="site-desktop-title top-bar-logo">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
          <?php get_template_part('dist/assets/images/inline', 'logo.svg'); ?>
        </a>
      </div>
    </div>
    <div class="top-bar-right">
      <?php foundationpress_top_bar_r(); ?>
      <?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) === 'topbar' ) : ?>
        <?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
      <?php endif; ?>
    </div>
    <button class="menu-hamburguer" data-toggle="offCanvasRight">
      <?php get_template_part('dist/assets/images/inline', 'menu.svg'); ?>
    </button>
  </nav>

  <div class="grid-container fixed">
    <!-- <button class="search-button" data-toggle="pesquisar"><i><?php get_template_part('dist/assets/images/inline', 'search.svg'); ?></i></button> -->
    <div class="reveal fast ease-in-out" id="pesquisar" data-reveal data-close-on-click="true" data-animation-in="slide-in-down" data-animation-out="slide-out-up">
      <?php get_template_part( 'template-parts/header-search' ); ?>
    </div>
  </div>
</header>
