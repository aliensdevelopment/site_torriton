<?php
 /*
    Blog Carrosel
 */
?>


<section data-aos="fade-up" data-aos-offset="500" data-aos-duration="900"  class="blog-home grid-container full">
	 <div class="grid-container">
			<div class="blog-wrap grid-x grid-margin-x">
				<div class="box-info blog cell small-4">
					<span class="line"></span>
					<h3 class="title">Últimas do Blog</h3>
					<a class="arrow-link" data-icon="a">
						<span data-icon="a"></span>
					</a>
				</div>
			</div>
		</div>

		<?php
  			$blog_posts = new WP_Query(
		    array(
		      'post_type' => 'post',
		      'posts_per_page' => 2,
					'paged' => get_query_var('paged'),
					'category_name' => 'blog'
				    )
		  ); ?>

		<div class="blog-carousel owl-carousel owl-theme">
			<?php if ( $blog_posts->have_posts() ) : ?>
            	<?php while ( $blog_posts->have_posts() ) : $blog_posts->the_post(); ?>

			<div class="blog-carousel-item">
				<!--<header style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/diadanoiva.jpg')" class="thumbnail"></header>
				<header class="thumbnail"><a  href="<?php the_permalink(); ?>"><?php getTheFirstImage(); ?></a></header>-->

				<header><a  href="<?php the_permalink(); ?>"><?php getTheFirstImage(); ?></a></header>
				<article class="content">
					<span class="date"><?php the_time('j \d\e F \d\e Y') ?></span>
					<a href="<?php the_permalink(); ?>"><p class="resume"><?php the_title(); ?></p></a>
				</article>
			</div>

            <?php endwhile;?>
        <?php endif; ?>


			<div class="blog-carousel-item">
				<header style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/diadanoiva.jpg')" class="thumbnail"></header>
				<article class="content">
						<span class="date">23 DE ABRIL DE 2018</span>
						<p class="resume">Mês das noivas está chegando: inspire-se!</p>
				</article>
			</div>
			<div class="blog-carousel-item">
				<header style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/diadanoiva.jpg')" class="thumbnail"></header>
				<article class="content">
						<span class="date">23 DE ABRIL DE 2018</span>
						<p class="resume">Mês das noivas está chegando: inspire-se!</p>
				</article>
			</div>
			<div class="blog-carousel-item">
				<header style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/diadanoiva.jpg')" class="thumbnail"></header>
				<article class="content">
						<span class="date">23 DE ABRIL DE 2018</span>
						<p class="resume">Mês das noivas está chegando: inspire-se!</p>
				</article>
			</div>

		<!-- Código anterior 
			<div class="blog-carousel-item">
				<header style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/diadanoiva.jpg')" class="thumbnail"></header>
				<article class="content">
						<span class="date">23 DE ABRIL DE 2018</span>
						<p class="resume">Mês das noivas está chegando: inspire-se!</p>
				</article>
			</div>
		-->
		</div>
</section>
