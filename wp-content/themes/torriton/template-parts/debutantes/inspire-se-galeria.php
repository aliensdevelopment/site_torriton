<?php
 /*
    Inspire-se
 */
?>

<div class="grid-container wrap-block-side full-height">
  <div class="grid-x grid-margin-x align-right block-content">

    <div class="small-12 medium-9">
      <h1 data-aos="fade-right" class="small-10 cell title-big title-inspire">
        INSPIRE-SE
      </h1>

      <div class="grid-x grid-padding-x block-dicas-bg block-content-right full-height">
        <div class="cell small-12 medium-6 box-infosub-int box-inspire">
          <span class="line line-branco"></span>
          <div class="text">
            <p>Confira as debutantes produzidas pelo Torriton e inspire-se! Aqui você encontra penteados para debutantes, maquiagem para debutantes e produções de arrasar! </p>
            <h2 class="title">Inspire-se com as debutantes produzidas no Torriton!</h2>
          </div>
        </div>

      <div class="cell small-5 box-infosub-int">
          <div class="btn-area block-btn-debutantes">
            <a class="button-white button--white debutantes" href="#consulte">Entre em contato 
             <i class="arrow-icon-white"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
            <br/>
            <a class="button-white button--white debutantes" href="#consulte">Consulte os preços
            <i class="arrow-icon-white"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<section data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">

    <div class="galeria transparent carrosel owl-carousel owl-theme ">

        <!-- Galeria Inspire-se -->
        <?php

          global $post;
          $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-debutantes' );

          $myposts = get_posts( $args );

          foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
            <div class="galeria-item">
              <a class="thumbnail">
                <!-- Verifica imagem -->
                <?php 
                  $image = get_field('foto_galeria');

                  if( !empty($image) ): ?>

                      <img onclick="galeriaModal(this)" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"  />

                  <?php endif; ?>
              </a>
            </div>
             <div class="galeria-item">
              <a class="thumbnail">
                <!-- Verifica imagem -->
                <?php 
                  $image = get_field('foto_galeria_1');

                  if( !empty($image) ): ?>

                      <img onclick="galeriaModal(this)" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"  />

                  <?php endif; ?>
              </a>
            </div>
             <div class="galeria-item">
              <a class="thumbnail">
                <!-- Verifica imagem -->
                <?php 
                  $image = get_field('foto_galeria_2');

                  if( !empty($image) ): ?>

                      <img onclick="galeriaModal(this)" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"  />

                  <?php endif; ?>
              </a>
            </div>
          <?php endforeach; 
          wp_reset_postdata();?>
    </div>
</section>
    <!-- Galeria -->
  <?php get_template_part( 'template-parts/galerias/galeria-foto' ); ?>
