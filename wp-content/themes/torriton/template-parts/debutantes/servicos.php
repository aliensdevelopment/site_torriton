<?php
  /*
    Servicos
  */
?>

 <section class="itens-wrap servicos servicos-debutantes grid-container">
    <div class="grid-x grid-margin-x align-right">
    <div class="small-9">
      <h3 class="t-info-title int">PACOTES</h3>
      <div class="grid-x itens-wrap-int align-justify">

        <?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'pacotes-debutantes' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                <?php if( get_field('titulo_pacote') ): ?>
                    <article data-aos="fade-up" data-aos-offset="250" data-aos-duration="1000" class="item-block cell noivas">
                        <header class="item-block-title debutantes"><?php the_field("titulo_pacote"); ?></header>
                <?php endif; ?>

                    <?php 
                        $image = get_field('foto_pacote');

                        if( !empty($image) ): ?>

                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="item-block-foto" />

                        <?php endif; ?>

                    <?php if( get_field('descricao_do_pacote') ): ?>
                        <div class="item-block-text">
                            <?php the_field("descricao_do_pacote"); ?>
                        </div>
                    <?php endif; ?>

                    <?php if( get_field('box_destaque') ): ?>
                        <div class="destaque-area-debutantes">
                            <p>
                              <?php the_field("box_destaque"); ?>
                            </p>
                        </div>
                    <?php endif; ?>

                    <?php if( get_field('detalhes_adicionais') ): ?>
                        <div class="item-block-text">
                            <?php the_field("detalhes_adicionais"); ?>
                        </div>
                    <?php endif; ?>

                    <?php if( get_field('botao_1') ): ?>
                      <div class="btn-area ">
                        <a class="button button--debutantes" href="<?php the_field("url_btn_1"); ?>"><?php the_field("botao_1"); ?>
                         <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
                      </div>
                    <?php endif; ?>

                    <?php if( get_field('botao_2') ): ?>
                      <div class="btn-area ">
                        <a class="button button--debutantes" href="<?php the_field("url_btn_1"); ?>"><?php the_field("botao_2"); ?>
                         <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
                      </div>
                    <?php endif; ?>



                </article>
            <?php endforeach; 
            // Fim do Laço
            wp_reset_postdata();?>

        </div>
    </div>
    </div>
 </section>
