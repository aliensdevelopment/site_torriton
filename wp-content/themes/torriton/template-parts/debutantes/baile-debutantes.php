<?php
/*
  Baile Debutantes
*/
?>

<div id="baile-debutantes" class="parallax-b cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/debutantes/baile.jpg"></div>
  <div class="wrap-info-fullhero c grid-container wrap-block-side full-height ">
    <div class="grid-x grid-margin-x align-right block-content">
      <div class="small-12 medium-9">
        <h1 data-aos="fade-right" class="small-5 branco cell title-big branco title-baile">
          BAILE DE </br>
          DEBUTANTES
        </h1>

        <div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
          <div class="cell small-12 medium-6 box-infosub-int">
            <span class="line"></span>
            <div class="text">
              <p>Você é debutante e vai fazer parte de um Baile de Debutantes? Conhece mais amigas que vão dividir essa data linda com você?  Fale conosco! Conseguimos descontos incríveis para grupos de debutantes e convidadas!</p>
              <p>É um Clube ou está organizando um baile de debutantes e quer fazer uma parceria? Fale conosco! Temos espaço para arrumar todas as debutantes e fornecer descontos e vantagens incríveis para deixar esse dia ainda mais especial ;)  </p>
            </div>
          </div>

          <div class="cell small-5 box-infosub-int">
            <article class="small-box box-button">
              <div class="btn-area block-btn-debutantes">
                <a class="button button--debutantes" href="#consulte">Entre em contato 
                 <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
                <br/>
                <a class="button button--debutantes" href="#consulte">Consulte os preços
                <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
              </div>
            </article>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php get_template_part( 'template-parts/blog/blog-horario' ); ?>
  <?php get_template_part( 'template-parts/forms/formulario' ); ?>
