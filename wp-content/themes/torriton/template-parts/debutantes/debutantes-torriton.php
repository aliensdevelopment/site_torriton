<?php
 /*
   Formandas Torriton
 */
?>

<section id="debutantes-torriton" class="wrap-frost-spacing debutantes">
  <div class="grid-container full wrap-block-side transparent">
    <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/debutantes/debutante-torriton.jpg')" class="foto-bg"></div>

    <div class="grid-x grid-margin-x align-center block-content">
      <h1 data-aos="fade-right" class="title-big">
        DEBUTANTE TORRITON
      </h1>

      <article class="box-info-int box-info-int-white cell small-6">
        <span class="line"></span>
          <div class="text">
            <p>Desde 1977 escolhemos, apoiamos e formamos as melhores equipes para atender as debutantes no salão de beleza.</p>
            <p>Somos referência no ramo de festas e pacotes para debutantes em Curitiba.</p>
        </div>
        <div class="btn-area">
          <a class="button button--debutantes" href="#consulte"> Entre em Contato
           <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
           <br/>

            <a class="button button--debutantes" href="#consulte"> Consulte os preços
           <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
        </div>
      </article>
    </div>
  </div>
</section>
