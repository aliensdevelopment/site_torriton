<?php
 /*
  Hero
 */
?>

<header class="hero-int" role="banner">
	<div data-aos="fade-right" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/debutantes/debutante.jpg')" class="foto-bg"></div>
	<section  id="15-anos" class="conteudo grid-x align-right">
        <h1 data-aos="fade-left" data-aos-duration="1200" class="title-big">
          DEBUTANTES
				</h1>
				<div class="box-info-int cell small-12 medium-6">
					<h2 class="title">MEUS 15 ANOS</h2>
					<div class="text">
						<p>15 anos, é uma idade muito especial e vai muito além de apenas uma desculpa para fazer festa. </p>
						<p>A festa de debutante é um momento incrível e que vai ficar na sua memória para sempre! </p>
						<p>É seu dia de brilhar, você é a estrela no show e todos vão estar lá para comemorar com você!</p>
						<p>Escolha aquele vestido de debutante dos sonhos, procure inspiração para penteado para debutante e escolha aquela make para debutante incrível, porque sempre que ver as fotos da sua festa de 15 anos você vai lembrar como estava linda e curtiu muito!</p>
						<p>“MAS Torriton, socorro!! Eu não sei por onde começar! Nem pensei em nada disso! ”</p>
						<p>Lindaaaa, você precisa ter a certeza que está nas mãos de quem sabe o que fala. Por isso, vamos além de entregar um pacote de beleza, entregamos dicas valiosas para o seu dia ser perfeito!</p>
					</div>
				</div>
	</section>
	<!--
	<section class="grid-x align-center">
		<div class="small-12 medium-4">
			<div class="box-info box-text">
				<span class="line"></span>
				<p class="text-left">
				Vem com a gente e vamos te ajudar a deixar esse dia memorável! </p>
			</div>
		</div>
	</section>
-->
</header>
