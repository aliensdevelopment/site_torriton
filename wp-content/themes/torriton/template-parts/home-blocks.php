<?php
 /*
    Blocos Home
 */
?>

<section class="itens-wrap servicos-home grid-container ha-waypoint" data-animate-down="ha-header-show" data-animate-up="ha-header-hide">
	<div class="grid-x home-item grid-margin-x align-justify">

	 <?php
        global $post;
        $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'servicos-torriton-home' );

        $myposts = get_posts( $args );

        // Início do Laço selecionado pelo nome da categoria
        foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
            <article data-aos="fade-up" data-aos-offset="300" data-aos-duration="1000" class="item-block cell noivas">
                <header class="item-block-title" style=" background-color:<?php the_field('cor_do_titulo'); ?>"><?php the_field("titulo_servico_destaque"); ?></header>

                <?php 
                    $image = get_field('foto_do_servico');

                    if( !empty($image) ): ?>

                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="item-block-foto" />

                    <?php endif; ?>

                <div class="item-block-text">
                    <?php the_field("descricao_do_servico"); ?>
                </div>

                <div class="item-block-text">
                    <a class="item-block-link" href="<?php the_field("link_home"); ?>"><?php the_field("texto_do_link"); ?>
                    <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
                </div>

            </article>
        <?php endforeach; 
        // Fim do Laço
        wp_reset_postdata();?>

	</div>
</section>
