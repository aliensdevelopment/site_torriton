<?php 
/* Página de Categoria */

get_header();
?>

<section class="section-categoria">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="medium-8">
				<?php while ( have_posts() ) : the_post(); ?>
					<div class="small-12 medium-6">
						<div class="post-item">
							<img src="<?php the_post_thumbnail_url('full'); ?>" />
							<article>
								<span class="data"><?php the_time('j \d\e F \d\e Y') ?></span>
								<a href="<?php echo get_permalink(); ?>">
								<h3 class="title"><?php the_title(); ?></h3></a>

							  <div class="text-post">
			                    <a class="t-info-text" href="<?php echo get_permalink(); ?>"><?php the_excerpt(); ?></a>
			                  </div>

								<footer class="group">
									<a class="group-item" href="<?php echo get_permalink(); ?>">Leia mais</a>
									<?php the_category( ' ' ); ?>
									<a class="group-item"><?php comments_number(); ?></a>
								</footer>
							</article>
						</div>
					</div>

				<?php endwhile; ?>
			</div>

			<div class="medium-4">
				<aside class=" cell small-12 medium-6">
					<div class="newsletter">
						<div class="box-info no-bg block blog cell small-12">
							<div class="matter-block">
								<h3 class="title">PROMOÇÕES & NEWS</h3>
								<article class="grid-x grid-margin-x">
									<div class="cell small-12">
										<div class="text">
											<p>Quer receber nosso conteúdo em primeira mão?</p>
										</div>
										<form>
											<div class="small-12 cell">
												<label>
													<input type="text" placeholder="Nome:">
												</label>
											</div>
											<div class="small-12 cell">
												<label>
													<input type="text" placeholder="E-mail:">
												</label>
											</div>
											<button class="submit item-block-link"><input type="submit" value="Enviar"><i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></button>
											</form>
									</div>
								</article>
							</div>
						</div>
					</div>

					<div class="categorias pg-single">
						<?php 
							if ( is_active_sidebar('sidebar-1') ) {
							    dynamic_sidebar('sidebar-1');
							}
						?>
					</div>

					<div class="arquivos pg-single">
						<h1 class="title">Arquivo</h1>

						<div class="selectdiv">
							<?php 
								if ( is_active_sidebar('sidebar-2') ) {
								    dynamic_sidebar('sidebar-2');
								}
							?>
						</div>
					</div>
			</aside>

			</div>

		</div>
	</div>
	
</section>

<?php get_footer();
