document.addEventListener('DOMContentLoaded', function () {
  var textarea = document.querySelector('.js-form textarea');
  if (textarea) {
    textarea.addEventListener('keydown', autosize);
    textarea.addEventListener('paste', autosize);
    textarea.addEventListener('cut', autosize);

    function autosize() {
      var el = this;
      setTimeout(function () {
        el.style.cssText = 'height:auto; padding:0';
        // for box-sizing other than "content-box" use:
        // el.style.cssText = '-moz-box-sizing:content-box';
        el.style.cssText = 'height:' + el.scrollHeight + 'px';
      }, 0);
    }
  }
});
