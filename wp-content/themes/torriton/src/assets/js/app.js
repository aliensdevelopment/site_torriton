import $ from 'jquery';
import jQuery from 'jquery';

/* Libs */
import './lib/waypoint';
import './lib/owl.carousel.min';
import './lib/smothscroll';
import './lib/parallax';
import './lib/md-textarea';
import AOS from './lib/aos';

import Foundation from 'foundation-sites';

window.$ = $;

$(document).foundation();

// Header Waypoints
var $head = $('#header');
$('.ha-waypoint').each(function (i) {
  var $el = $(this),
    animClassDown = $el.data('animateDown'),
    animClassUp = $el.data('animateUp');

  $el.waypoint(function (direction) {
    if (direction === 'down' && animClassDown) {
      $head.attr('class', 'ha-header ' + animClassDown);
    } else if (direction === 'up' && animClassUp) {
      $head.attr('class', 'ha-header ' + animClassUp);
    }
  }, {
    offset: '10%'
  });
});

AOS.init({
  duration: 600,
  delay: 100,
});

// Carrosel
$(document).ready(function () {
  $('.blog-carousel').owlCarousel({
    center: true,
    loop: false,
    margin: 10,
    autoWidth: true,
    nav:false,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 3,
        nav: false
      },
      1000: {
        items: 5,
        nav: true,
        loop: false
      }
    }
  });

  $('.marcas-carousel').owlCarousel({
    center: true,
    items:4,
    loop:false,
    margin:10,
    nav:false,
    responsive:{
        600:{
            items:7
        }
    }
  });

  $('.galeria').owlCarousel({
    center: true,
    items:4,
    loop:false,
    margin:10,
    nav:false,
    responsive:{
        600:{
            items:7
        }
    }
  });

  $('.depoimentos-carousel').owlCarousel({
    items: 1,
    loop: true,
    margin: 30,
    nav:false,
    smartSpeed: 500
  });

  var $contactForm = $('.js-form');
  var $input = $contactForm.find('input');
  var $textarea = $contactForm.find('textarea');
  var parent;
  if ($contactForm) {
    $($input).focusin(function () {
      parent = this.parentElement.parentElement;
      parent.classList.add('active');
    });
    $($input).focusout(function () {
      parent = this.parentElement.parentElement;
      if (this.value === '' || this.value === '__-_____-____') parent.classList.remove('active');
    });

    $($textarea).focusin(function () {
      parent = this.parentElement.parentElement;
      parent.classList.add('active');
    });
    $($textarea).focusout(function () {
      parent = this.parentElement.parentElement;
      if (this.value === '') parent.classList.remove('active');
    });
  }


});

// Parallax
$('.parallax-window').parallax({
  naturalWidth: 1920,
  naturalHeight: 744,
  speed: 0.6
});

$('.parallax-b').parallax({
  naturalWidth: 1920,
  naturalHeight: 695,
  speed: 0.6
});

$('.parallax-c').parallax({
  naturalWidth: 1920,
  naturalHeight: 611,
  speed: 0.7
});

$('.parallax-d').parallax({
  naturalWidth: 1920,
  naturalHeight: 582,
  speed: 0.6
});

// Arquivos/blog
$("select").click(function () {
  var open = $(this).data("isopen");
  if (open) {
    window.location.href = $(this).val()
  }
  $(this).data("isopen", !open);
});
