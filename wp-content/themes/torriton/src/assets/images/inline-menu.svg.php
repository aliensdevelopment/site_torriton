<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 13.25 12.4">
  <defs>
    <style>
      .menu-icon-1 {
        fill: none;
      }

      .menu-icon-2 {
        clip-path: url(#clip-path);
      }

      .menu-icon-3 {
        fill: #fff;
      }
    </style>
    <clipPath id="clip-path">
      <rect class="menu-icon-1" width="13.25" height="12.4"/>
    </clipPath>
  </defs>
  <g id="menu-icon" transform="translate(-1527 -36)">
    <g class="menu-icon-2" transform="translate(1527 36)">
      <g transform="translate(-1527 -13)">
        <path class="menu-icon-3" d="M0,0H13V2H0Z" transform="translate(1527 13)"/>
      </g>
      <g transform="translate(-1527 -8)">
        <path class="menu-icon-3" d="M0,0H13V2H0Z" transform="translate(1527 13)"/>
      </g>
      <g transform="translate(-1527 -3)">
        <path class="menu-icon-3" d="M0,0H13V2H0Z" transform="translate(1527 13)"/>
      </g>
    </g>
  </g>
</svg>
