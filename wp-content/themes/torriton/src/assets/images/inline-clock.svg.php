<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.336 18.336">
  <defs>
    <style>
      .icon-clock {
        fill: #fff;
      }
    </style>
  </defs>
  <g id="icon-clock" data-name="icon-clock" transform="translate(0)">
    <path class="icon-clock" d="M9.168,18.336A9.168,9.168,0,1,0,0,9.168,9.174,9.174,0,0,0,9.168,18.336Zm0-16.894A7.726,7.726,0,1,1,1.442,9.168,7.736,7.736,0,0,1,9.168,1.442Z"/>
    <path class="icon-clock" d="M182.459,103.756l3.088,2.4a.768.768,0,0,0,.445.156.7.7,0,0,0,.565-.276.713.713,0,0,0-.132-1.009l-2.8-2.187V97.9a.721.721,0,1,0-1.442,0v5.287A.718.718,0,0,0,182.459,103.756Z" transform="translate(-174.457 -93.062)"/>
  </g>
</svg>
