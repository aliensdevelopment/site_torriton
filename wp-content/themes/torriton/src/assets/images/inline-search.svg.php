<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32.555 33.342">
  <defs>
    <style>
      .icon-search {
        fill: #fff;
        stroke: #fff;
      }
    </style>
  </defs>
  <g id="icon-search" transform="translate(0.5 0.5)">
    <g class="icon-search" data-name="Group 59" transform="translate(0 0)">
      <path class="icon-search" data-name="Path 111" class="icon-search" d="M30.821,22.891a13.461,13.461,0,1,0-1.83,1.666l7.571,7.415a1.218,1.218,0,0,0,.864.37,1.184,1.184,0,0,0,.864-.37,1.254,1.254,0,0,0,0-1.748ZM9.658,13.636A11.168,11.168,0,1,1,20.826,24.8,11.178,11.178,0,0,1,9.658,13.636Z" transform="translate(-7.19 0)"/>
    </g>
  </g>
</svg>
