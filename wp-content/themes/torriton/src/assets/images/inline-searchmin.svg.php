<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.383 13.675">
  <defs>
    <style>
      .icon-searchmin {
        fill: #fff;
        stroke: #fff;
      }
    </style>
  </defs>
  <g id="icon-searchmin" transform="translate(-320.5 37.5)">
    <g transform="translate(321 -37)">
      <path class="icon-searchmin" d="M16.451,8.971a5.275,5.275,0,1,0-.717.653L18.7,12.53a.477.477,0,0,0,.339.145.464.464,0,0,0,.339-.145.491.491,0,0,0,0-.685ZM8.157,5.344A4.377,4.377,0,1,1,12.534,9.72,4.381,4.381,0,0,1,8.157,5.344Z" transform="translate(-7.19 0)"/>
    </g>
  </g>
</svg>
