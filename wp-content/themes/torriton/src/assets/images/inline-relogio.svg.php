<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.469 43.47">
  <defs>
    <style>
      .icon-relogio {
        fill: #a1a1a1;
      }
    </style>
  </defs>
  <g id="Group_92" data-name="Group 92" transform="translate(0 0)">
    <path id="Path_165" data-name="Path 165" class="icon-relogio" d="M21.735,43.47A21.735,21.735,0,1,0,0,21.735,21.749,21.749,0,0,0,21.735,43.47Zm0-40.051A18.316,18.316,0,1,1,3.418,21.735,18.339,18.339,0,0,1,21.735,3.418Z"/>
    <path id="Path_166" data-name="Path 166" class="icon-relogio" d="M182.838,112.765l7.321,5.7a1.822,1.822,0,0,0,1.054.37,1.659,1.659,0,0,0,1.339-.655,1.691,1.691,0,0,0-.313-2.393L185.6,110.6V98.892a1.709,1.709,0,1,0-3.418,0v12.534A1.7,1.7,0,0,0,182.838,112.765Z" transform="translate(-163.867 -87.412)"/>
  </g>
</svg>
