<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 8.942 14.6">
  <defs>
    <style>
      .icon-arrow-white {
        fill: #fff;
      }
    </style>
  </defs>
  <g id="icon-arrow-white" transform="translate(-1549.516 -878.571)">
    <rect class="icon-arrow-white" width="2" height="10" transform="translate(1557.044 884.685) rotate(45)"/>
    <path class="icon-arrow" d="M0,0H2V10H0Z" transform="translate(1558.001 885.642) rotate(135)"/>
  </g>
</svg>
