<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.445 21.728">
  <defs>
    <style>
      .icon-marker {
        fill: #fff;
      }
    </style>
  </defs>
  <g id="icon-marker" transform="translate(0)">
    <g transform="translate(0)">
      <path class="icon-marker" d="M169.024,114.467a3.415,3.415,0,1,0,3.415,3.415A3.415,3.415,0,0,0,169.024,114.467Zm0,5.3a1.886,1.886,0,1,1,1.886-1.886A1.89,1.89,0,0,1,169.024,119.768Z" transform="translate(-161.302 -109.319)"/>
      <path class="icon-marker" d="M83.235,2.549A7.651,7.651,0,0,0,77.335,0c-4.919,0-7.493,4-7.493,7.952,0,5.174,7.073,13.164,7.379,13.508a.74.74,0,0,0,.561.268h.013a.8.8,0,0,0,.561-.255A38.024,38.024,0,0,0,81.8,17.051c2.319-3.466,3.492-6.537,3.492-9.112A8.229,8.229,0,0,0,83.235,2.549ZM77.807,19.765c-1.657-1.988-6.423-8.016-6.423-11.826,0-3.084,1.873-6.41,5.964-6.41a6.274,6.274,0,0,1,6.41,6.41C83.758,12.425,79.387,17.917,77.807,19.765Z" transform="translate(-69.842)"/>
    </g>
  </g>
</svg>
