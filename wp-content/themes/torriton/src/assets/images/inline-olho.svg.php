<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 55.833 35.145">
  <defs>
    <style>
      .icon-olho {
        fill: #a1a1a1;
      }
    </style>
  </defs>
  <g id="eye-view" transform="translate(0 0)">
    <g id="Group_86" data-name="Group 86" transform="translate(0 0)">
      <path id="Path_160" data-name="Path 160" class="icon-olho" d="M6.5,115.979a29.9,29.9,0,0,0,21.421,8.647,30.034,30.034,0,0,0,21.421-8.647,31.253,31.253,0,0,0,6.321-8.712,1.883,1.883,0,0,0,0-1.572A26.713,26.713,0,0,0,49.5,97.506c-3.963-3.668-10.972-8.025-21.781-8.025S10.033,93.87,6.136,97.538A25.317,25.317,0,0,0,.142,105.76a1.855,1.855,0,0,0,.033,1.507A30.463,30.463,0,0,0,6.5,115.979ZM27.622,93.542a9.564,9.564,0,1,1-9.564,9.564A9.575,9.575,0,0,1,27.622,93.542Zm-11.1,1.9a13.38,13.38,0,0,0-2.391,7.664,13.494,13.494,0,1,0,24.532-7.73A24.9,24.9,0,0,1,51.6,106.513a29.662,29.662,0,0,1-5.175,6.813,25.66,25.66,0,0,1-18.539,7.435,25.823,25.823,0,0,1-18.539-7.468,29.774,29.774,0,0,1-5.208-6.846A24,24,0,0,1,16.519,95.442Z" transform="translate(0.002 -89.481)"/>
    </g>
  </g>
</svg>
