<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22.334 22.334">
  <defs>
    <style>
      .icon-facebook {
        fill: #fff;
      }
    </style>
  </defs>
  <path id="icon-facebook" class="icon-facebook" d="M11.167,22.334A11.17,11.17,0,0,1,6.82.878a11.17,11.17,0,0,1,8.693,20.579A11.1,11.1,0,0,1,11.167,22.334ZM9.632,11.016V16.6h2.034V11.016h1.526l.3-1.746H11.666V8.005c0-.4.257-.836.641-.836h1.039V5.424H12.072v.008a2.446,2.446,0,0,0-1.952.791,2.674,2.674,0,0,0-.485,1.646h0v1.4H8.615v1.746Z" transform="translate(0)"/>
</svg>
