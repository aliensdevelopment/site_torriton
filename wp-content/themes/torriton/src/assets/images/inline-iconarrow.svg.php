<svg class="arrow-right-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 8.942 14.6">
  <g transform="translate(-1549.516 -878.571)">
    <rect width="2" height="10" transform="translate(1557.044 884.685) rotate(45)"/>
    <path d="M0,0H2V10H0Z" transform="translate(1558.001 885.642) rotate(135)"/>
  </g>
</svg>