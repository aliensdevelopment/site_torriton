<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<section>
  <div class="grid-container">
    <div class="single grid-x grid-margin-x block-content">
			<?php while ( have_posts() ) : the_post(); ?>

				<div class="small-12 medium-12">
					<header class="featured-thumb" role="banner">
						<img src="<?php the_post_thumbnail_url('full'); ?>" />
					</header>
				</div>

	      		<div class="cell small-12 medium-7 blogposts-wrapper">
						<article class="blogposts-item single">
							<span class="data"><?php the_time('j \d\e F \d\e Y') ?></span>
							<h3 class="title"><?php the_title(); ?></h3>
							<?php the_content(); ?>
						</article>
						
				</div>
			<?php endwhile; ?>

			<aside class="cell small-12 medium-5">
				<div class="newsletter">
					<div class="box-info no-bg block blog cell small-12">
						<div class="matter-block">
							<h3 class="title">PROMOÇÕES & NEWS</h3>
							<article class="grid-x grid-margin-x">
								<div class="cell small-12">
									<div class="text">
										<p>Quer receber nosso conteúdo em primeira mão?</p>
									</div>
										<form>
											<div class="small-12 cell">
												<label>
													<input type="text" placeholder="Nome:">
												</label>
											</div>
											<div class="small-12 cell">
												<label>
													<input type="text" placeholder="E-mail:">
												</label>
											</div>
											<button class="submit item-block-link"><input type="submit" value="Enviar"><i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></button>
										</form>
								</div>
							</article>
						</div>
					</div>
				</div>

				<div class="categorias pg-single">
					<?php 
						if ( is_active_sidebar('sidebar-1') ) {
						    dynamic_sidebar('sidebar-1');
						}
					?>
				</div>

				<div class="arquivos pg-single">
					<h1 class="title">Arquivo</h1>

					<div class="selectdiv">
						<?php 
							if ( is_active_sidebar('sidebar-2') ) {
							    dynamic_sidebar('sidebar-2');
							}
						?>
					</div>
				</div>
			</aside>
		</div>
	</div>

	<?php get_template_part( 'template-parts/blog/blog-carossel' ); ?>
</section>

<?php get_template_part( 'template-parts/mapa-locais' ); ?>

<?php get_footer();
