<?php
/*
Template Name: Formandas
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-formandas' ); ?>
</div>

<section id="formandas">

  <section id="section1" class="cd-section">
    <?php get_template_part( 'template-parts/formandas/hero-inicio' ); ?>
  </section>

  <section id="section1" class="cd-section">
    <?php get_template_part( 'template-parts/formandas/formandas-torriton' ); ?>
    <?php get_template_part( 'template-parts/formandas/servicos' ); ?>
  </section>
</section>

<?php get_footer();
