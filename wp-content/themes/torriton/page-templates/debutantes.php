<?php
/*
Template Name: Debutantes
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-debutantes' ); ?>
</div>

<section id="debutantes">

  <section id="section1" class="cd-section">
    <?php get_template_part( 'template-parts/debutantes/hero-inicio' ); ?>
  </section>

  <section id="section1" class="cd-section">
    <?php get_template_part( 'template-parts/debutantes/debutantes-torriton' ); ?>
    <?php get_template_part( 'template-parts/debutantes/servicos' ); ?>
  </section>
</section>
<?php get_template_part( 'template-parts/forms/formulario' ); ?>

<?php get_footer();
