<?php
/*
  Template Name: Torriton Men
*/
get_header(); ?>

<div class="container">
	<?php get_template_part( 'template-parts/shared/sidenav-men' ); ?>
</div>

<section id="sobre" class="container-spacing">
	<div class="grid-container full wrap-block-side torriton-men-wrapper">
		<div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/men/torriton-men-01.jpg')" class="foto-bg estetico-torriton-bg"></div>
		<div class="grid-container wrap-block-side transparent full-height">
			<div class="grid-x grid-margin-x align-center block-content">
				<div class="medium-5 small-12">
					<h1 data-aos="fade-right" class="small-10 cell t-title-diamond estetico-torriton-title">
						Torriton Men
					</h1>
					<div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
						<div class="box-info-int box-info-dica cell small-12">
							<span class="line"></span>
							<p class="t-info-text">
								Há muito tempo deixou de ser um tabu homens que prezam pela boa aparência. O cuidado com o cabelo e a barba virou uma rotina na vida do homem moderno.
							</p>
							<p class="t-info-text">
								Pensando no conforto e nas necessidades dos nossos clientes surge o Torriton Men. O espaço fica localizado dentro da unidade do Shopping Pátio Batel e é conhecido por apresentar um novo conceito de salão masculino em Curitiba.
							</p>
							<p class="t-info-text">
								Torriton Men é o aliado de homens modernos e elegantes.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_template_part( 'template-parts/torriton-men/torriton-men' ); ?>

<div id="inspire-se" class="grid-container full wrap-block-side full-height">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class=" small-12 medium-12">
				<div class="grid-x grid-padding-x ">
					<div class="medium-12 medium-offset-4 align-right bloco-wish">
						<div id="" class="text-area">
							<h1 data-aos="fade-right" class="text-left t-title-diamond ">Inspire-se</h1>
							<p class="t-info-text">
								Nossos profissionais são especializados em todos os tipos de corte de cabelo e barbearia. Dê uma olhada nas fotos de cortes de cabelo masculino que foram feitas pela equipe do Torriton!
							</p>
							<p class="t-social">
								Siga nas redes sociais.
							</p>
							<a href="#">
							<i><?php get_template_part('dist/assets/images/icon', 'fb.svg'); ?></i>
							</a>

							<a href="#">
								<i><?php get_template_part('dist/assets/images/icon', 'instagram.svg'); ?></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<section data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">
		<div class="galeria int carrosel owl-carousel owl-theme ">

			<?php

				global $post;
				$args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-men' );

				$myposts = get_posts( $args );

				foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
					<div class="galeria-item">
						<a class="thumbnail">
							<!-- Verifica imagem -->
							<?php 
								$image = get_field('foto_galeria');

								if( !empty($image) ): ?>

										<img onclick="galeriaModal(this)" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"  />

								<?php endif; ?>
						</a>
					</div>
				<?php endforeach; 
				wp_reset_postdata();?>
		</div>
</section>


<?php get_template_part( 'template-parts/galerias/galeria-foto' ); ?>

<div id="dicas">
	<?php get_template_part( 'template-parts/blog/blog-horario' ); ?>
</div>

<?php get_template_part( 'template-parts/nails/mapa-nails' ); ?>

<?php get_footer();

