<?php
/*
  Template Name: Torriton Store
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-store' ); ?>
</div>

<section id="torriton-store" >
<div class="grid-container full wrap-block-side">
  <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/store/store.jpg')" class="foto-bg">
  </div>
  <div class="grid-container">
    <div class="grid-x grid-margin-x">
      <div class=" small-12 medium-12">
        <div class="grid-x grid-padding-x ">
          <div class="medium-6 medium-offset-4 align-right bloco-store">
            <h1  data-aos="fade-right" class="text-left t-title-diamond ">Torriton Store</h1>
            <div id="sobre" class="text-area">
              <span class="purple-line"></span>
                <p class="t-info-text">
                    Nós sabemos que você está cansada de ouvir que nuuuunca vai conseguir
                    aquele resultado de cabelo perfeito pós salão lavando as madeixas em casa.
                    Mas nós te garantimos, os produtos da Torriton Store são essenciais para manter a saúde dos fios em casa e deixá-los sempre deslumbrantes. Trabalhamos com as melhores marcas profissionais do mercado.
                </p>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <?php // Marcas Torriton Store ?>
  <?php get_template_part( 'template-parts/store/marcas' ); ?>

  <?php // Galeria Maquiagem ?>
  <div class="grid-container full wrap-block-side full-height">
   <div data-aos="fade-right" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/store/hot-makeup.jpg');height: 70vh;" class="foto-bg bg-left make-store">
   </div>

    <div id="hot-make-up" class="grid-container">
      <div class="grid-x grid-margin-x">
        <div class=" small-12 medium-12">
          <div class="grid-x grid-padding-x ">
            <div class="medium-12 medium-offset-4 align-right bloco-acessorios">
              <div class="text-area">
                   <h1  data-aos="fade-right" class="text-left t-title-diamond ">Hot make up</h1>
                  <p class="t-info-text">
                     Vinda diretamente de Los Angeles, a americana Hot Makeup aterrissa no Brasil com muitas novidades: novos produtos, novas embalagens e apresenta seu mais badalado lançamento, a Linha Rose Gold – uma inovadora proposta de maquiagem saudável que proporciona às mulheres revelar uma beleza marcante e natural.
                  </p>

                  <p class="t-social">
                      Siga nas redes sociais.
                  </p>
                  <a href="https://www.facebook.com/hotmakeupprofessional/" target="_blank">
                   <i><?php get_template_part('dist/assets/images/icon', 'fb.svg'); ?></i>
                  </a>

                  <a href="https://www.instagram.com/hotmakeup.bra/" target="_blank">
                   <i><?php get_template_part('dist/assets/images/icon', 'instagram.svg'); ?></i>
                 </a>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section id="carrossel-make" data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">
    <div class="galeria carrosel-store transparent carrosel owl-carousel owl-theme">
        <?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-hot-make-up' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                <?php 

                $images = get_field('galeria');

                if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                            <div class="galeria-item">
                              <img onclick="galeriaModal(this)" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />
                             </div>
                        <?php endforeach; ?>
                <?php endif; ?>

            <?php endforeach; 
            // Fim do Laço
          wp_reset_postdata();?>
    </div>
    </section>
  <!-- Fim Galeria Make -->

  <!-- Galeria Wish -->
  <div id="acessorios" class="grid-container full wrap-block-side full-height">
    <div class="grid-container">
      <div class="grid-x grid-margin-x">
        <div class=" small-12 medium-12">
          <div class="grid-x grid-padding-x ">
            <div class="medium-12 medium-offset-4 align-right bloco-wish">
              <div class="text-area">
                   <h1 data-aos="fade-right" class="text-left t-title-diamond ">Wish acessórios</h1>
                  <p class="t-info-text">
                     Você ainda pode se apaixonar pelas Semi Joias da Wish Acessórios, trazendo o que há de melhor em tendências para deixar o seu Look ainda mais luxuoso!
                  </p>

                  <p class="t-social">
                      Siga nas redes sociais.
                  </p>
                  <a href="https://www.facebook.com/Wish-Acess%C3%B3rios-450928195289214/" target="_blank">
                   <i><?php get_template_part('dist/assets/images/icon', 'fb.svg'); ?></i>
                  </a>

                  <a href="https://www.instagram.com/wish_semijoias/" target="_blank">
                   <i><?php get_template_part('dist/assets/images/icon', 'instagram.svg'); ?></i>
                 </a>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <section data-aos="fade-left" data-aos-delay="500" data-aos-duration="900" class="grid-container full">

    <div class="galeria carrosel-store transparent carrosel owl-carousel owl-theme">
        <?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-wish' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                <?php 

                $images = get_field('galeria');

                if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                            <div class="galeria-item">
                              <img onclick="galeriaModal(this)" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />
                             </div>
                        <?php endforeach; ?>
                <?php endif; ?>

            <?php endforeach; 
            // Fim do Laço
          wp_reset_postdata();?>
    </div>
    </section>
</section>
  <?php get_template_part( 'template-parts/galerias/galeria-foto' ); ?>
<?php get_template_part( 'template-parts/blog/blog-horario' ); ?>

<section id="mapa-store">
  <?php get_template_part( 'template-parts/mapa-locais' ); ?>
</section>
<?php get_footer();