<?php
/*
Template Name: Salões e Quiosques - Pátio Batel
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-saloes' ); ?>
</div>

<section id="saloes-e-quiosques">
  <?php get_template_part( 'template-parts/saloes-e-quiosques/patio-batel' ); ?>
  <?php get_template_part( 'template-parts/blog/blog-promocao' ); ?>
</section>

<section id="mapa-batel">
  <?php get_template_part( 'template-parts/mapa-locais' ); ?>
</section>
<?php get_footer();