<?php
/*
Template Name: Contato
*/
get_header(); ?>

  <section class="contact">
		<div class="grid-container">
			<div class="grid-x align-center">
				<div class="contact_wrapper">
					<div class="contact_wrapper__header">
						<h1 class="t-title-diamond">CONTATO</h1>
						<p class="t-info-text">
							Por favor, preencha o formulário abaixo. 
							Todos os campos são obrigatórios
						</p>
					</div>
					<div class="contact_wrapper__form js-form">

						<?php 
							echo do_shortcode('[contact-form-7 id="11416" title="Contato"]'); 
						?>

						<!--
							////////////////////// SOURCE CODE FOR CONTACT FORM 7 PANEL
							* Plugin: Contact Form 7
							* Plugin Extension: Contact Form 7 - Phone mask field

							<div class="group form-group">      
								[text* your-name class:form-control]
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Nome:</label>
							</div>

							<div class="group form-group">      
								[email* your-email class:form-control]
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>E-mail:</label>
							</div>

							<div class="group form-group">      
								[mask* your-phone class:form-control "__-_____-____"]
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Telefone com DDD:</label>
							</div>

							<div class="group form-group--textarea">
								[textarea* your-message class:form-control rows:1]
								<span class="highlight"></span>
								<span class="bar"></span>
								<label>Mensagem:</label>
							</div>

							<div class="group form-group">
								<div class="checkbox">
									[checkbox checkbox-211 default:1 "Desejo receber emails promocionais do Torriton."]
								</div>
							</div>

							[submit class:button class:button--purple "Enviar"] 
						
						-->
				</div>
			</div>
  </section>

  <?php get_template_part( 'template-parts/mapa-locais' ); ?>

<?php get_footer();