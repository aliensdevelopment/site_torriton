<?php
/*
  Template Name: Responsabilidade Social
*/
get_header(); ?>

	<div class="container">
		<?php get_template_part( 'template-parts/shared/sidenav-responsabilidade' ); ?>
	</div>

	<section id="responsabilidade" class="container-spacing">
		<div class="grid-container full wrap-block-side estetico-torriton-container">
			<div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/responsabilidade/responsabilidade-01.jpg')" class="foto-bg"></div>
			<div class="grid-container wrap-block-side transparent full-height">
				<div class="grid-x grid-margin-x align-center block-content">
					<div class="medium-5 small-12">
						<h1 data-aos="fade-right" class="small-10 cell t-title-diamond estetico-torriton-title">
							Responsabilidade Social
						</h1>
						<div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
							<div class="box-info-int box-info-dica cell small-12">
								<span class="line"></span>
								<p class="t-info-text">
									A beleza também está presente em ser solidário e fazer o bem!
								</p>
								<p class="t-info-text">
									O Torriton valoriza as iniciativas que promovem ajudar o próximo e a
									solidariedade, além disso levamos muito a sério nossa responsabilidade social.
									O salão de beleza desenvolve atividades em parceria com os profissionais e
									instituições beneficentes! Quer fazer uma parceria para projetos sociais com o
									Torriton? Então entre em contato conosco!
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="parcerias">
		<div class="parallax-c cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/responsabilidade/responsabilidade-03.jpg"></div>
		<div class="wrap-info-fullhero c grid-container wrap-block-side full-height">
			<div class="grid-x grid-margin-x align-right block-content estetico-torriton-centro">
				<div class="small-9">
				<h1 data-aos="fade-right" class="small-5 branco cell t-title-diamond">
					PARCERIAS
				</h1>

				<div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
					<div class="box-info-int full-width box-info-dica cell medium-8 small-12">
						<p class="t-info-text">
							Participamos junto com a APADEVI – Associação de Pais e Amigos de
							Deficientes Visuais e a Instituição Pequeno Cotolengo em práticas sustentáveis
							e ações sociais.
						</p>
						<p class="t-info-text">
							O Torriton também realiza uma festa de encerramento, onde profissionais,
							funcionários e colaboradores são convidados a incentivar e arrecadar doações,
							brinquedos e produtos de higiene pessoal que são distribuídos em instituições
							solidarias, entre elas o LISA - Lar Infantil Sol Amigo, a Sociedade Espírita Capa
							dos Pobres e a Fundação Betânia.
						</p>
						<p class="t-info-text">
							Além disso, sabemos a importância da beleza na vida e principalmente na
							auto-estima das mulheres. Por essa razão, desde 2014, a ação beneficente
							que tem obtido um retorno enorme é o Outubro Rosa, proporcionada pelo salão
							de beleza Torriton em parceria com o projeto Atitude na Cabeça!
						</p>
						<h1 id="doacao" data-aos="fade-right" class="small-5 cell t-title-diamond title-02">
							Doação de Cabelos
						</h1>
						<p class="t-info-text">
							Desde de 2014 o Torriton realizada a campanha de Outubro Rosa para doação
							de cabelos em prol de mulheres e crianças com câncer. Os cortes são gratuitos
							durante todo o mês de segunda a quarta.
						</p>
						<p class="t-info-text">
							Em 2017, em parceria com o projeto Atitude na Cabeça e a empresa Cabify,
							entrou junto na campanha de Outubro Rosa que arrecadou mais de 350
							doações de cabelo para a confecção de perucas feitas com cabelos humanos
							para pacientes com câncer de mama!
						</p>
						<p class="t-info-text">
							Porém, não é apenas durante o Outubro Rosa que aceitamos doações de
							cabelo! Você pode vir a qualquer época do ano fazer uma doação, com 20% no
							valor do corte*!
						</p>
						<p class="t-info-text">
							*Demais serviços, incluindo lavado e cachos, são cobrados no valor integral.
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="parallax-window cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/responsabilidade/responsabilidade-02.jpg">
		<div class="noivas-video-content grid-container full">
			<div class="grid-x grid-margin-x align-middle align-center">
				<a class="cell icon-play" href="#"><?php get_template_part('dist/assets/images/inline', 'play.svg'); ?></a>
			</div>
		</div>
	</div>

	<section id="reconhecimento">
		<div class="parallax-c cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/responsabilidade/responsabilidade-04.jpg"></div>
		<div class="wrap-info-fullhero c grid-container wrap-block-side full-height">
			<div class="grid-x grid-margin-x align-right block-content estetico-torriton-centro">
				<div class="small-9">
				<h1 data-aos="fade-right" class="small-5 branco cell t-title-diamond">
					RECONHECIMENTO
				</h1>

				<div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
					<div class="box-info-int full-width box-info-dica cell medium-8 small-12">
						<p class="t-info-text">
							Certificado de apoio à Campanha Natal Solidário para crianças em situação de
							vulnerabilidade | FAZ – Fundação de Ação Social 2017
						</p>
						<p class="t-info-text">
							Participação beneficente na festa de Natal | APADEVI - Associação de Pais e
							Amigos de Deficientes Visuais 2016	
						</p>
						<p class="t-info-text">
							Certificado de incentivo à cultura curitibana | Tesão Piá 2015
						</p>
						<p class="t-info-text">
							Apoio ao sorteio realizado para Festa Junina | Colégio Suíço Brasileiro 2015
						</p>
						<p class="t-info-text">
							Certificado de participação do Natal Encantado do Paraná | Governo Estadual
							do Paraná, Provopar Estadual e CANAL/mkt 2014
						</p>
						<p class="t-info-text">
							Certificado de participação na Campanha Doe Calor | FAZ - Fundação de Ação
							Social e Instituto Pró-Cidadania de Curitiba 2014
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="modulo-blog" data-aos="fade-up" data-aos-offset="100" data-aos-duration="900" class="blog-int grid-container full">
	<div class="grid-x grid-padding-x align-right block-content">
		<div class="small-12 medium-9 blog-pos">
			<div class="grid-container">
	 			<div class="blog-wrap  grid-x grid-margin-x">
	 				<div class="box-info block blog cell small-12 medium-9 large-7">
	 					<article class="grid-x grid-margin-x">
	 						<div class="cell small-6">
	 							<div class="text">
	 								<h2 class="title-dicas"> Blog
									</h2>
									<span class="line-blog"></span>
	 							</div>
	 							<div class="text">
	 								<p>Conheça mais sobre a nossa Responsabilidade Social</p>
	 							</div>
	 						</div>

							<div class="newsletter-blog">
								<div class="box-info block blog small-4">
									<div class="matter-block">
										<article class="grid-x grid-margin-x">
											<div class="cell small-12">
												<div class="text">
													<p>Cadastre-se na newsletter e receba dicas inscríveis para o seu dia! </p>
												</div>
													<form>
												      <div class="small-12 cell">
												        <label>
												          <input type="text" placeholder="Nome:">
												        </label>
												      </div>
												      <div class="small-12 cell">
												        <label>
												          <input type="text" placeholder="E-mail:">
												        </label>
												      </div>
															<button class="submit item-block-link"><input type="submit" value="Enviar"><i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></button>
													</form>
											</div>
										</article>
									</div>
								</div>
							</div>
	 					</article>
	 				</div>
	 			</div>
	 		</div>

		    <!-- Blog -->
				<?php
		  			$blog_posts = new WP_Query(
				    array(
				      'post_type' => 'post',
				      'posts_per_page' => 6,
							'paged' => get_query_var('paged'),
							'category_name' => 'blog'
						    )
				  ); ?>

			 		<div class="blog-carousel owl-carousel owl-theme">

			 		<?php if ( $blog_posts->have_posts() ) : ?>
		            	<?php while ( $blog_posts->have_posts() ) : $blog_posts->the_post(); ?>

					<div class="blog-carousel-item">

						<a href="<?php the_permalink(); ?>"><header class="thumbnail" style=" background-image: url('<?php the_post_thumbnail_url('full'); ?>');" >
						</header></a>
						<article class="content">
							<span class="date"><?php the_time('j \d\e F \d\e Y') ?></span>
							<a href="<?php the_permalink(); ?>"><p class="resume"><?php the_title(); ?></p></a>
						</article>
					</div>

		            <?php endwhile;?>
		        <?php endif; ?>

	 		</div>
		</div>
	</div>
</section>

<?php get_footer();
