<?php
// Template Name: Serviços Torriton
get_header(); ?>

  <section id="tabela-servicos">
    <div class="grid-container">
      <div class="grid-x grid-padding-x block-content">
        <div class="medium-12">
          <h2 class="t-title-diamond title-promocoes">Serviços fornecidos torriton</h2>
          <ul class="tabs locais" data-tabs id="servicos-torriton">
            <li class="tabs-title is-active local-title"><a href="#maquiagem" class="no-anchor" aria-selected="true"><i ><?php get_template_part('dist/assets/images/icon', 'make.svg'); ?></i> Maquiagem</a></li>

            <li class="tabs-title local-title"><a data-tabs-target="cabelo" class="no-anchor" href="#cabelo"><i ><?php get_template_part('dist/assets/images/icon', 'cabelo.svg'); ?></i> Cabelo</a></li>

            <li class="tabs-title local-title"><a href="#homem" class="no-anchor" aria-selected="true"><i ><?php get_template_part('dist/assets/images/icon', 'homem.svg'); ?></i> Homem</a></li>

            <li class="tabs-title local-title"><a data-tabs-target="maos" class="no-anchor" href="#maos"><i ><?php get_template_part('dist/assets/images/icon', 'maos.svg'); ?></i> Mãos e pés</a></li>

            <li class="tabs-title local-title"><a data-tabs-target="estetica" class="no-anchor" href="#estetica"><i ><?php get_template_part('dist/assets/images/icon', 'estetica.svg'); ?></i> Estética</a></li>
          </ul>

          <!-- Início Conteúdo Tabs -->
          <div class="tabs-content tabs-locais" data-tabs-content="servicos-torriton">

            <div class="tabs-panel is-active" id="maquiagem">
             <?php get_template_part( 'template-parts/saloes-e-quiosques/tabelas/t_maquiagem' ); ?>
            </div>
            <div class="tabs-panel" id="cabelo">
             <?php get_template_part( 'template-parts/saloes-e-quiosques/tabelas/t_cabelo' ); ?>
            </div>
            <div class="tabs-panel" id="homem">
             <?php get_template_part( 'template-parts/saloes-e-quiosques/tabelas/t_homem' ); ?>
            </div>
            <div class="tabs-panel" id="maos">
             <?php get_template_part( 'template-parts/saloes-e-quiosques/tabelas/t_maos-e-pes' ); ?>
            </div>
            <div class="tabs-panel" id="estetica">
             <?php get_template_part( 'template-parts/saloes-e-quiosques/tabelas/t_estetica' ); ?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php get_footer();?>
</section>