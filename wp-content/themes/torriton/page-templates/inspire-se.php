<?php
/*
Template Name: Inspire-se
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-formandas' ); ?>
</div>

<section id="formandas" class="sub-page">

  <?php get_template_part( 'template-parts/formandas/inspire-se-galeria' ); ?>

  <?php get_template_part( 'template-parts/formandas/depoimentos' ); ?>

  <?php get_template_part( 'template-parts/formandas/blogs-e-dicas' ); ?>

  <?php get_template_part( 'template-parts/formandas/duvidas' ); ?>
</section>

<?php get_footer();
