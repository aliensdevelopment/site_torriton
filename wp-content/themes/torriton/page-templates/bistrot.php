<?php
/*
  Template Name: Bistrot
*/
get_header(); ?>

	<div class="container">
		<?php get_template_part( 'template-parts/shared/sidenav-bistrot' ); ?>
	</div>

	<section >
		<div class="parallax-c cd-section" data-parallax="scroll" data-image-src="<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/bistrot/bistrot-01.png"></div>
		<div class="wrap-info-fullhero c grid-container wrap-block-side full-height">
			<div id="sobre" class="grid-x grid-margin-x align-right block-content">
				<div class="small-9">
				<h1 data-aos="fade-right" class="small-5 branco cell t-title-diamond">
					Bistrot
				</h1>

				<div class="grid-x grid-padding-x block-bistrot-bgwhite align-justify">
					<div class="box-info-int full-width box-info-dica cell medium-8 small-12">
						<p class="t-info-text">
							Além dos mais variados Cortes de cabelo masculino, o Torriton Men é conceito
							e referência no ramo de barbearia em Curitiba, oferecendo serviços clássicos
							de cuidados e beleza para o público masculino.
						</p>
						<p class="t-info-text">
							Oferecemos também todos os serviços estéticos para o homem que presa pela
							elegância e conforto.
						</p>
						<a href="#" class="item-block-link">Confira aqui todos os serviços oferecidos <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
					</div>
				</div>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/galerias/galeria-foto' ); ?>
	<section id="bistrot-1">
	<div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/bistrot/bistrot-02.png')" class="foto-bg"></div>
	<div class="wrap-info-fullhero e grid-container wrap-block-side full-height">
		<div class="grid-x grid-margin-x align-right block-content">
			<div class="small-9">
				<div class="grid-x grid-padding-x block-bistrot-bgwhite align-justify">
					<div class="cell small-12 box-infosub-int">
						<p class="t-info-text">
							Desfrute dos melhores serviços e profissionais do ramo de beleza e ainda
							aproveite pratos à la carte deliciosos no Torriton Bistrot! Além disso ainda
							contamos com um espaço de disposição de vinhos que você pode escolher
							para acompanhar sua comida ou para presentear alguém!
						</p>
						<p class="t-info-text">
							De terça a sábado, das 09h às 20h no salão Torriton Pres. Taunay, você pode
							desfrutar um almoço, café ou jantar fresquinhos enquanto fica linda!
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

	<section data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">
		<div class="galeria int carrosel owl-carousel owl-theme ">
		<?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-bistrot' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                <?php 

                $images = get_field('galeria');

                if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                            <div class="galeria-item">
                              <img onclick="galeriaModal(this)" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />
                             </div>
                        <?php endforeach; ?>
                <?php endif; ?>

            <?php endforeach; 
            // Fim do Laço
          wp_reset_postdata();?>
		</div>
	</section>
	<?php get_template_part( 'template-parts/galerias/galeria-foto' ); ?>

	<section id="cardapio" class="container-spacing">
		<div class="grid-container full wrap-block-side estetico-torriton-container">
			<div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/bistrot/bistrot-03.png')" class="foto-bg"></div>
			<div class="grid-container wrap-block-side transparent full-height">
				<div class="grid-x grid-margin-x align-center block-content">
					<div class="medium-5 small-12">
						<h1 data-aos="fade-right" class="small-10 cell t-title-diamond estetico-torriton-title">
							Cardápio
						</h1>
						<div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
							<div class="box-info-int box-info-dica cell small-12">
								<span class="line"></span>
								<p class="t-info-text">
									Veja nossas opções de pratos a seguir. Venha nos visitar e conhecer nosso espaço!
								</p>
								<h2 class="title t-info-title">
									Pratos Feitos | Saladas | Massas | Omeletes | Sanduíches
									Salgados | Sobremesas  | Refrescos | Cafés
								</h2>
								<a href="<?php the_field("cardapio_bistrot",11632); ?>" target="_blank" class="item-block-link">Clique aqui e confira nosso cardápio <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php get_template_part( 'template-parts/blog/blog-horario' ); ?>
	<?php get_template_part( 'template-parts/mapa-locais' ); ?>
<?php get_footer();