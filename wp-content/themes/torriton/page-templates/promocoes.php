<?php
/*
Template Name: Promoções
*/
get_header(); ?>

<section class="promocoes">
  <div class="grid-container">
    <div class="grid-x grid-margin-x block-content">
      <div class="cell small-12 medium-7 blogposts-wrapper">
        <h2 class="t-title-diamond title-promocoes">Promoções</h2>
        <ul class="tabs locais" data-tabs id="example-tabs">
          <li class="tabs-title is-active local-title"><a href="#panel1" class="no-anchor" aria-selected="true">Presidente Taunay</a></li>
          <li class="tabs-title local-title"><a data-tabs-target="panel2" class="no-anchor" href="#panel2">Pátio Batel</a></li>
          <li class="tabs-title local-title"><a data-tabs-target="panel3" class="no-anchor" href="#panel3">Sete de Setembro</a></li>
        </ul>

        <div class="tabs-content tabs-locais" data-tabs-content="example-tabs">
          <div class="tabs-panel is-active" id="panel1">
            <address class="endereco">
              PRES. TAUNAY, 321 - (41) 3091 8686
            </address>

            <!-- Exibe as postagens por categoria -  Promoções Taunay -->
            <?php

            if ( have_posts() ) :
            while ( have_posts() ) : the_post();
              global $post;

              $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'promocoes-taunay' );

              $myposts = get_posts( $args );

              foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                <article class="promocao">
                  <header>
                    <h2 class="promocao-title"><?php the_field('titulo_da_promocao'); ?></h2>
                    <legend class="promocao-legend"><?php the_field('texto_da_promocao'); ?></legend>
                    <span class="promocao-small"><?php the_field('validade_da_promocao'); ?></span>
                  </header>

                  <!-- Pega a imagem -->
                  <?php 
                        $image = get_field('foto_da_promocao');

                        if( !empty($image) ): ?>

                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                        <?php endif; ?>
                </article>
              <?php endforeach; 
              wp_reset_postdata();
            endwhile;

          else :
            echo wpautop( 'Não existe posts para esta categoria!' );
          endif;
          ?>

          </div>

          <div class="tabs-panel" id="panel2">
            <address class="endereco">
              PATIO. BATEL, 321 - (41) 3091 8686
            </address>



            <!-- Exibe as postagens por categoria -  Promoções Batel -->
            <?php

            if ( have_posts() ) :
            while ( have_posts() ) : the_post();
              global $post;

              $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'promocoes-batel' );

              $myposts = get_posts( $args );

              foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                <article class="promocao">
                  <header>
                    <h2 class="promocao-title"><?php the_field('titulo_da_promocao'); ?></h2>
                    <legend class="promocao-legend"><?php the_field('texto_da_promocao'); ?></legend>
                    <span class="promocao-small"><?php the_field('validade_da_promocao'); ?></span>
                  </header>

                  <!-- Pega a imagem -->
                  <?php 
                        $image = get_field('foto_da_promocao');

                        if( !empty($image) ): ?>

                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                        <?php endif; ?>
                </article>
              <?php endforeach; 
              wp_reset_postdata();
            endwhile;

          else :
            echo wpautop( 'Não existe posts para esta categoria!' );
          endif;
          ?>

          </div>

          <div class="tabs-panel" id="panel3">
            <address class="endereco">
              Sete de Setembro, 321 - (41) 3091 8686
            </address>

            <!-- Exibe as postagens por categoria -  Promoções Sete de Setembro -->
            <?php

            if ( have_posts() ) :
            while ( have_posts() ) : the_post();
              global $post;

              $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'promocoes-sete-de-setembro' );

              $myposts = get_posts( $args );

              foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                <article class="promocao">
                  <header>
                    <h2 class="promocao-title"><?php the_field('titulo_da_promocao'); ?></h2>
                    <legend class="promocao-legend"><?php the_field('texto_da_promocao'); ?></legend>
                    <span class="promocao-small"><?php the_field('validade_da_promocao'); ?></span>
                  </header>

                  <!-- Pega a imagem -->
                  <?php 
                        $image = get_field('foto_da_promocao');

                        if( !empty($image) ): ?>

                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                        <?php endif; ?>
                </article>
              <?php endforeach; 
              wp_reset_postdata();
            endwhile;

          else :
            echo wpautop( 'Não existe posts para esta categoria!' );
          endif;
          ?>

          </div>
        </div>
      </div>

      <aside class="sidebar cell small-12 medium-5">
        <div class="newsletter">
          <div class="box-info box-promocao no-bg block blog cell small-12">
            <div class="matter-block">
              <h3 class="title">PROMOÇÕES & NEWS</h3>
              <article class="grid-x grid-margin-x">
                <div class="cell small-12">
                  <div class="text">
                    <p>Quer receber nosso conteúdo em primeira mão?</p>
                  </div>
                    <form>
                        <div class="small-12 cell">
                          <label>
                            <input type="text" placeholder="Nome:">
                          </label>
                        </div>
                        <div class="small-12 cell">
                          <label>
                            <input type="text" placeholder="E-mail:">
                          </label>
                        </div>
                        <button class="submit item-block-link"><input type="submit" value="Enviar"><i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></button>
                      </form>
                </div>
              </article>
            </div>
          </div>
        </div>
      </aside>
    </div>
  </div>
</section>

<?php get_template_part( 'template-parts/mapa-locais' ); ?>

<?php get_footer();
