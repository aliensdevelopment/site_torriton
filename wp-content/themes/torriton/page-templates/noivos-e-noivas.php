<?php
/*
Template Name: Noivos e Noivas
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-noivxs' ); ?>
</div>
<section id="noivas-e-noivos" class="sub-page">

  <?php get_template_part( 'template-parts/noivas-noivos/intro' ); ?>

</section>

<?php get_footer();
