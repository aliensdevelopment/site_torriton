<?php
/*
Template Name: Salões e Quiosques - Quiosque L'oreal
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-saloes' ); ?>
</div>

<section id="saloes-e-quiosques" class="subpage">
  <?php get_template_part( 'template-parts/saloes-e-quiosques/quiosque' ); ?>
  <?php get_template_part( 'template-parts/blog/blog-promocao' ); ?>
</section>

<section id="mapa-quiosque">
  <?php get_template_part( 'template-parts/mapa-locais' ); ?>
</section>
<?php get_footer();