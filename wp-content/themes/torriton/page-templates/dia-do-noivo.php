<?php
/*
Template Name: Dia do Noivo
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-noivxs' ); ?>
</div>

<section id="noivas-e-noivos" class="sub-page">

  <?php get_template_part( 'template-parts/noivas-noivos/hero-noivo' ); ?>

  <section id="consultoria">
    <?php get_template_part( 'template-parts/noivas-noivos/consultoria' ); ?>
    <div class="frost-wrapper consultoria"></div>
  </section>


  <section class="wrap-color-full">
    <div class="grid-container wrap-block-side transparent padding-reset full-height">
      <div class="grid-x grid-margin-x align-right block-content">
        <div class="small-9">
          <div class="grid-x grid-padding-x block-dicas-bg align-justify">
            <div class="box-info-int box-info-dica cell small-12">
              <span class="line line-branco"></span>
              <h2 class="title">Dica 11:</h2>
              <div class="text dica">
                <p>“A escolha de uma equipe de profissionais exclusivos para o seu casamento deve ser sua.”</p>
              </div>
              <article class="medium-box">
                <h2 class="title">Cada serviço precisa do seu tempo e das equipes certas. O nosso dever é escutar e entender o seu estilo, desejos, e lhe mostrar os trabalhos, as tendências e as qualidades das nossas equipes. Como salão precisamos primeiramente saber se tem preferências. </h2>
                <div class="text">
                  <p>Se não tiver preferência ou indicações, deixe a gente conhecer melhor o seu estilo, os seus sonhos, as suas exigências, as suas expectativas. Saberemos assim lhe mostrar os melhores perfis técnicos entre as melhores e experientes equipes. Como? É o trabalho da nossa consultoria gratuita do Dia da Noiva em Curitiba. 
                  Descubra e Planeje quais tempos e quais equipes você precisa para lhe atender como você quer. Como? Na consulta gratuita do Dia da Noiva Torriton!</p>
                </div>
              </article>
            </div>
            <div class="medium-6">
              <div class="btn-area">
                  <a class="button button-white noivas wrap-left" href="">Conheça os profissionais
                    <i class="arrow-icon-white"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>

                  <a class="button button-white noivas wrap-left" href="">Entre em contato
                    <i class="arrow-icon-white"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>

                  <a class="button button-white noivas wrap-left" href="">Consulte os preços
                    <i class="arrow-icon-white"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <div class="grid-container wrap-block-side transparent full-height padding-b-reset">
    <div class="grid-x grid-margin-x align-right block-content">
      <div class="small-9">
        <div class="grid-x grid-padding-x block-dicas-bgwhite top-spacing align-justify">
          <div class="box-info-int box-info-dica cell small-6">
            <span class="line"></span>
            <h2 class="title">Dica 12:</h2>
            <div class="text dica">
              <p>“Quer contratar ou contratou um cerimonial?"</p>
              </div>
          </div>

          <div class="cell small-5 box-infosub-int">
            <article class="small-box">
              <h2 class="title">Ótimo, seremos o melhor aliado que ele pode imaginar</h2>
              <div class="text">
                <p>Se acha que contratar um bom cerimonial significa deixar de lado o estresse da organização e durante o seu dia, você pode estar certa, mas descobrirá somente quando é tarde demais ou seja no mesmo dia do seu casamento. A partir da plataforma de gerenciamento dos convidados, e pelos motivos especificados acima podemos ajudar o cerimonial a manter o controle com o resultado que o seu dia terá mais chances de sair como você sempre sonhou!
                Não contratou e quer contratar? Bom nós temos bastante experiência e colaboramos com muitos. Quem melhor do que a gente para lhe dar dicas? Fica a dica e agende com a gente uma consultoria gratuita para o seu Dia da Noiva em Curitiba.
                </p>
              </div>
              <div class="btn-area">
                <a class="button button--noivas" href="">Entre em contato
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>

                <a class="button button--noivas" href="">Consulte preços
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
              </div>
            </article>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="grid-container full">
    <div class="grid-x grid-padding-x">
      <div class="small-12 medium-6 large-6">
        <div data-aos="fade-right" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/recomendacao.jpg')" class="foto-bg foto-bg-box"></div>
      </div>
      <div class="small-12 medium-6 large-6">
        <div class="grid-x grid-padding-x">
          <div class="small-12 ">
            <div class="box-conteudo">
              <div>
                <p>
                  Uma última recomendação...
                </p>
                <p>
                  Feche com quem acha melhor, mas não antes de se informar a fundo com mais de um salão competente! Agende agora sem nenhum compromisso uma consultoria gratuita sobre o dia da noiva e o dia do noivo Torriton. Muita informação valiosa! Máxima transparência! É o nosso trabalho desde 1977. 
                </p>
                <p>
                  Não se arrependa! Se o casamento vai ser pela vida não vai ter outra chance! Está achando que vai ter mais chances na vida para ter um dia da noiva digno de ser lembrado? Espere aí, vai ter outro casamento na sua vida? Tem mesmo certeza que vai quer casar agora?!
                </p>
                 <p>
                  Com muito amor e carinho, equipe de consultoria do dia da noiva e noivo da Rede de salões Torriton 
                </p>
              </div>
              <div class="btn-area">
                <a class="button button-white noivas" href="">Entre em contato
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>

                <a class="button button-white noivas" href="">Consulte preços
                        <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
              </div>
            </div>
            
          </div>

        </div>
      </div>
    
  </div>
</section>

<?php get_template_part( 'template-parts/forms/formulario' ); ?>
<?php get_footer();