<?php
/*
Template Name: Infraestrutura
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-noivxs' ); ?>
</div>

<section id="noivas-e-noivos" class="sub-page">

  <?php get_template_part( 'template-parts/noivas-noivos/infraestrutura' ); ?>

  <?php get_template_part( 'template-parts/noivas-noivos/galeria-infraestrutura' ); ?>

  <?php get_template_part( 'template-parts/noivas-noivos/bistro' ); ?>

  <?php get_template_part( 'template-parts/noivas-noivos/sala-noiva' ); ?>

  <?php get_template_part( 'template-parts/noivas-noivos/galeria-salanoivas' ); ?>

</section>

<?php get_template_part( 'template-parts/forms/formulario' ); ?>
<?php get_footer();
