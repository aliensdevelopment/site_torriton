<?php
/*
  Template Name: Quem Somos
*/
get_header(); ?>

	<div class="container">
		<?php get_template_part( 'template-parts/shared/sidenav-quem-somos' ); ?>
	</div>

	<section id="quem-somos" class="container-spacing">
		<div class="grid-container full wrap-block-side estetico-torriton-container">
			<div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/quem-somos/quem-somos-1.jpg')" class="foto-bg"></div>
			<div class="grid-container wrap-block-side transparent full-height">
				<div class="grid-x grid-margin-x align-center block-content">
					<div class="medium-5 small-12">
						<h1 data-aos="fade-right" class="small-10 cell t-title-diamond estetico-torriton-title">
							Quem Somos
						</h1>
						<div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
							<div class="box-info-int box-info-dica cell small-12">
								<span class="line"></span>
								<p class="t-info-text">
									O Torriton é uma rede de salões de beleza que assina personalidade e
									sofisticação em suas unidades.
									Os salões Torriton são referência no mercado por unir vanguarda e elegância
									ao atendimento de profissionais renomados na área da beleza e bem-estar.
								</p>
								<p class="t-info-text">
									Com três unidades localizadas no bairro batel (Curitiba/PR), o Torriton é
									considerado o queridinho da mulher moderna e poderosa.
								</p>
								<p class="t-info-text">
									Em 2017 a rede completou 40 anos de história. Inovou com excelência o
									mercado de beleza em Curitiba e foi eleito um dos 20 melhores salões de
									beleza do Brasil (Revista Estilo – Grupo Abril).
								</p>
								<p class="t-info-text">
									Além de oferecer um espaço destinado a beleza, o Torriton oferece uma gama
									de facilidades e ambientes diferenciados alinhando submarcas que agregam
									valor a marca.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="conheca">
		<div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/quem-somos/quem-somos-2.jpg')" class="foto-bg conheca-bg"></div>
		<div class="wrap-info-fullhero e grid-container wrap-block-side full-height">
			<div class="grid-x grid-margin-x align-right block-content">
				<div class="small-9 quem-somos-text">
					<h1 class="t-title-diamond small-5 cell estetico-servicos-title">Conheça</h1>
					<div class="grid-x grid-padding-x block-dicas-bgwhite align-justify"></div>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/galerias/galeria-foto' ); ?>
	<section data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">
		<div class="galeria int carrosel owl-carousel owl-theme ">
			<?php
				global $post;
				$args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-quem-somos' );

				$myposts = get_posts( $args );
				
				foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
			 <?php

                for ($i = 1; $i<=10; ++$i) {
					
						$image = get_field('foto_da_pagina_'.$i);
						if( !empty($image) ): ?>
						<div class="galeria-item">
							<a href="<?php the_field("link_da_pagina_".$i);?>" class="thumbnail">
								<div class="thumbnail__wrapper">
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
								</div>
							</a>
							<div>
								<h2 class="title"><?php the_field("nome_da_pagina_".$i); ?></h2>
							</div>
						</div>
					<?php endif; ?>
              <?php } ?>

				<?php endforeach; 
				wp_reset_postdata();?>
		</div>
	</section>

	<section  id="visao-missao">
		<div class="grid-container full wrap-block-side">
			<div data-aos="fade-right" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/quem-somos/quem-somos-3.jpg')" class="foto-bg"></div>
			<div class="grid-container wrap-block-side transparent full-height">
				<div class="grid-x grid-margin-x align-center block-content">
					<div class="medium-5 small-12">
						<div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
							<div class="box-info-int box-info-dica cell small-12">
								<span class="line"></span>
								<p class="t-info-text">
									VISÃO<br>
									Integração das áreas de beleza, moda e bem-estar. Sociabilização e
									intercâmbio de conceitos de modernidade, elegância, glamour e luxo em
									espaços amplos e sofisticados.
								</p>
								<p class="t-info-text">
									MISSÃO<br>
									Com grande competência, paixão e entusiasmo, buscamos valorizar a beleza,
									moda e bem-estar de cada cliente. Busca constante das tendências e das
									competências. Atenção a excelência no serviço. A mais importante referência
									no setor da beleza, da moda e do bem-estar.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer();
