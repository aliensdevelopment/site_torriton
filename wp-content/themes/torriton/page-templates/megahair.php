<?php
/*
Template Name: Megahair
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-megahair' ); ?>
</div>

<section id="megahair">

  <section id="section1" class="cd-section">
    <?php get_template_part( 'template-parts/megahair/hero-inicio' ); ?>
  </section>

  <section id="section1" class="cd-section">
    <?php get_template_part( 'template-parts/megahair/megahair-torriton' ); ?>
  </section>

  <section id="section1" class="cd-section">
    <?php get_template_part( 'template-parts/megahair/vantagens-megahair' ); ?>
  </section>

  <section id="section1" class="cd-section">
    <?php get_template_part( 'template-parts/megahair/tecnicas-megahair' ); ?>

    <?php get_template_part( 'template-parts/megahair/tecnicas' ); ?>
  </section>
</section>

<?php get_footer();
