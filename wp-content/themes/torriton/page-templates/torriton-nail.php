<?php
/*
  Template Name: Torriton Nail
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-nails' ); ?>
</div>

<section id="sobre" class="container-spacing">
    <div class="grid-container full wrap-block-side estetico-torriton-container">
      <div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/nails/nail.jpg')" class="foto-bg estetico-torriton-bg"></div>
      <div class="grid-container wrap-block-side transparent full-height">
        <div class="grid-x grid-margin-x align-center block-content">
          <div class="medium-5 small-12">
            <h1 data-aos="fade-right" class="small-10 cell t-title-diamond estetico-torriton-title">
            Torriton Nail
            </h1>
            <div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
              <div class="box-info-int box-info-dica cell small-12">
              <h2 class="title t-info-title">SOBRE</h2>
                <p class="t-info-text">
                Inspirado nas tendências de Nail Bar que surgiram por todo o mundo, o Torriton desenvolveu um espaço dedicado exclusivamente para os serviços de manicure e pedicure: o Torriton Nail, presente nas unidades Pres. Taunay e Shopping Pátio Batel.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

    <div class="grid-container full wrap-block-side gray-int-matter full-height">
      <div data-aos="fade-right" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/nails/nail-salao.jpg')" class="foto-bg bg-left bg-nail"></div>
        <div class="grid-container wrap-block-side transparent full-height no-padding" >
          <div class="grid-x grid-margin-x align-right block-content">
            <div class="medium-6 small-12">
              <div class="grid-x grid-padding-x block-dicas-bgwhite block-margin block-info-nail align-justify">
                <div class="box-info-int box-info-dica cell small-12">
                  <span class="line"></span>
                  <p class="t-info-text">Arquitetura assinada pelo arquiteto Arthur Galliari da Costa, com conceitos e design diferenciado, proporcionando ainda mais conforto, tranquilidade e uma experiência única para os clientes.</p>
                  <p class="t-info-text">Conte com uma equipe de manicures especializadas em todos os tipos de unhas e oferecem diversas marcas de esmalte e opções de cores de esmalte. O ambiente foi pensado exclusivamente para o cuidado com as unhas.</p>
                  <p class="t-info-text">Aproveite para relaxar! Longe do barulho dos secadores e conversas do salão, nosso espaço Nail contempla poltronas confortáveis e ambiente super clean.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

<?php get_template_part( 'template-parts/nails/servicos-nails' ); ?>
<?php get_template_part( 'template-parts/nails/galeria-nail' ); ?>
</section>

<?php get_template_part( 'template-parts/blog/blog-horario' ); ?>
<section id="mapa-store">
  <?php get_template_part( 'template-parts/mapa-locais' ); ?>
</section>
<?php get_footer();