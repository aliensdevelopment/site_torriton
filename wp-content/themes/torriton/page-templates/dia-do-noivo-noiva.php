<?php
/*
Template Name: Dia do Noivo / Noiva
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-noivxs' ); ?>
</div>

<section id="noivas-e-noivos">

  <section id="" class="cd-section">
    <?php get_template_part( 'template-parts/noivas-noivos/hero-inicio' ); ?>
  </section>

  <section id="video" class="cd-section">
    <?php get_template_part( 'template-parts/noivas-noivos/hero-video' ); ?>
  </section>

  <section class="cd-section">
    <?php get_template_part( 'template-parts/noivas-noivos/intro' ); ?>
  </section>
</section>

<?php get_template_part( 'template-parts/forms/formulario' ); ?>
<?php get_footer();
