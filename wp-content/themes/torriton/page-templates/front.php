<?php
/*
Template Name: Front
*/
get_header(); ?>

<header class="home-hero" role="banner">
	<section class="conteudo grid-x align-middle">

	    <?php
            global $post;
            $args = array( 'posts_per_page' => 1, 'offset'=> 0, 'category_name' => 'destaque-home' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>


               <?php 
                $image = get_field('foto_destaque');

                if( !empty($image) ): ?>

                     <div class="foto-bg" style="background-image: url(<?php echo $image['url']; ?>);" data-aos="fade-left" data-aos-duration="800"  data-aos-once="true">
                    </div>

                <?php endif; ?>


				<h1 data-aos="fade-right" data-aos-duration="800" data-aos-once="true" class="title-big">
					<?php the_field("texto"); ?>
				</h1>

				<div data-aos="fade-left" data-aos-duration="1200" data-aos-once="true" class="box-info cell small-12 medium-6">
					<span class="line"></span>
					<p class="text text-home"><?php the_field("texto_box"); ?></p>
					<a href="<?php the_field("link_do_box"); ?>" class="arrow-link" data-icon="a">
						<span data-icon="a"></span>
					</a>
			</div>


        <?php endforeach; 
        // Fim do Laço
        wp_reset_postdata();?>

	</section>
</header>

	<?php get_template_part( 'template-parts/home-blocks' ); ?>

	<?php get_template_part( 'template-parts/blog/blog-promocao' ); ?>

	<?php get_template_part( 'template-parts/mapa-locais' ); ?>
 


<?php get_footer();
