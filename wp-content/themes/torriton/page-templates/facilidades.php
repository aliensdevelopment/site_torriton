<?php
// Template Name: Facilidades
get_header(); ?>


  <section id="tabela-servicos">
    <div class="grid-container">
     <div class="grid-x grid-padding-x block-content">
        <div class="medium-12">
          <h2 class="t-title-diamond title-promocoes">Facilidades torriton</h2>
          <!-- Início Conteúdo  -->

            <!-- Exibe a Tabela  Facilidades-->
            <?php 

            $table = get_field( 'tabela_facilidades' );

              if ( $table ) {

                  echo '<table border="0">';

                      if ( $table['header'] ) {

                          echo '<thead>';

                              echo '<tr>';

                                  foreach ( $table['header'] as $th ) {

                                      echo '<th class="th-title">';
                                          echo $th['c'];
                                      echo '</th>';
                                  }

                              echo '</tr>';

                          echo '</thead>';
                      }

                      echo '<tbody>';

                          foreach ( $table['body'] as $tr ) {

                              echo '<tr>';

                                  foreach ( $tr as $td ) {

                                      echo '<td class="td-text">';
                                          echo $td['c'];
                                      echo '</td>';
                                  }

                              echo '</tr>';
                          }

                      echo '</tbody>';

                  echo '</table>';
              }

            ?>
        </div>
     </div>
    </div>
  </div>

    <?php get_footer();?>
</section>