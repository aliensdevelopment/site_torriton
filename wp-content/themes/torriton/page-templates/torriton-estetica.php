<?php
/*
  Template Name: Torriton Estética
*/
get_header(); ?>

	<div class="container">
		<?php get_template_part( 'template-parts/shared/sidenav-estetica' ); ?>
	</div>
	<section id="sobre" class="container-spacing">
		<div class="grid-container full wrap-block-side estetico-torriton-container">
			<div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/estetica/torriton-estetica-01.jpg')" class="foto-bg estetico-torriton-bg"></div>
			<div class="grid-container wrap-block-side transparent full-height">
				<div class="grid-x grid-margin-x align-center block-content">
					<div class="medium-5 small-12">
						<h1 data-aos="fade-right" class="small-10 cell t-title-diamond estetico-torriton-title">
							TORRITON ESTÉTICA
						</h1>
						<div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
							<div class="box-info-int box-info-dica cell small-12">
								<span class="line"></span>
								<p class="t-info-text">
								Saúde e beleza são fundamentais e andam lado a lado. Aqui no Torriton levamos essas duas questões muito a sério e por esse motivo em todas nossas unidades é possível encontrar um setor exclusivo de estética. Contando com profissionais especializados. Além disso, foi desenvolvido um espaço para um centro de estética exclusivo para a realização de tratamentos estéticos dentro da unidade Presidente Taunay!
								</p>
								<a href="#" class="item-block-link">Conheça o Torriton Taunay. <i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrow.svg'); ?></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php get_template_part( 'template-parts/torriton-estetica/centro-estetico-torriton' ); ?>
	<?php get_template_part( 'template-parts/torriton-estetica/torriton-estetico-servicos' ); ?>
	<?php get_template_part( 'template-parts/blog/blog-horario' ); ?>

	<?php get_template_part( 'template-parts/mapa-locais' ); ?>

<?php get_footer(); 
