<?php
/*
Template Name: Antes e Depois
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-megahair' ); ?>
</div>

<section id="megahair" class="sub-page">

  <?php get_template_part( 'template-parts/megahair/inspire-se-galeria' ); ?>

  <?php get_template_part( 'template-parts/megahair/depoimentos' ); ?>

  <?php get_template_part( 'template-parts/megahair/duvidas' ); ?>
</section>

<?php get_footer();
