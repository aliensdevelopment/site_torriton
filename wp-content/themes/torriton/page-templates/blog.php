<?php
/*
Template Name: Blog
*/
get_header(); ?>

<header class="hero-int matter" role="banner">
	<div data-aos="fade-in" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/blog/hero-blog.jpg')" class="foto-bg"></div>
  	<section class="conteudo grid-x align-left">
  		<div class="box-info-int cell small-6">
            <h1 class="t-title-diamond">Blog</h1>
            <span class="line"></span>
            <div class="text">
              <p class="black">Acompanhe nosso olhar sobre as tendências e dicas de moda e beleza.</p>
            </div>
  		</div>
  	</section>
</header>

<?php
  $blog_posts = new WP_Query(
    array(
      'post_type' => 'post',
      'posts_per_page' => 6,
			'paged' => get_query_var('paged'),
			'category_name' => 'blog'
    )
  ); ?>
<section class="blogposts">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="medium-8">
				<div class="small-12 medium-8">
					<?php if ( $blog_posts->have_posts() ) : ?>
			            <?php while ( $blog_posts->have_posts() ) : $blog_posts->the_post(); ?>
			            	<div class="post-item">
								<a href="<?php echo get_permalink(); ?>">
									<img src="<?php the_post_thumbnail_url('full'); ?>" /></a>
								<article>
									  <a class="data"><?php the_time('j \d\e F \d\e Y') ?></a>
					                  <h3 class="title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
					                  <div class="desc">
					                    <a class="t-info-text" href="<?php echo get_permalink(); ?>"><?php the_excerpt(); ?></a>
					                  </div>
									<footer class="group">
										<?php the_category( ' ' ); ?>
										<div>
											<a class="button button--blog" href="<?php echo get_permalink(); ?>">Leia mais
											<i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></a>
										</div>
									</footer>
				                </article>
				            </div>
			            <?php endwhile;?>
			        <?php endif; ?>

					<?php wp_pagenavi( array( 'query' => $blog_posts ) ); ?>
				</div>
			</div>
			<div class="medium-4" >
				<aside class="sidebar-blog box-promo cell small-12 medium-5">
					<div class="newsletter">
						<div class="box-info block blog cell small-12">
							<div class="matter-block">
								<span class="line line-branco"></span>
								<h3 class="title">PROMOÇÕES & NEWS</h3>
								<article class="grid-x grid-margin-x">
									<div class="cell small-12">
										<div class="text">
											<p>Quer receber nosso conteúdo em primeira mão?</p>
										</div>
											<form>
										      <div class="small-12 cell">
										        <label>
										          <input type="text" placeholder="Nome:">
										        </label>
										      </div>
										      <div class="small-12 cell">
										        <label>
										          <input type="text" placeholder="E-mail:">
										        </label>
										      </div>
													<button class="submit item-block-link"><input type="submit" value="Enviar"><i class="arrow-icon"><?php get_template_part('dist/assets/images/inline', 'iconarrowwhite.svg'); ?></i></button>
												</form>
									</div>
								</article>
							</div>
						</div>
					</div>

					<div class="categorias">
						<?php 
							if ( is_active_sidebar('sidebar-1') ) {
							    dynamic_sidebar('sidebar-1');
							}
						?>
					</div>

					<div class="arquivos">
						<h1 class="title">Arquivo</h1>

						<div class="selectdiv">
							<?php 
								if ( is_active_sidebar('sidebar-2') ) {
								    dynamic_sidebar('sidebar-2');
								}
							?>
						</div>
					</div>
				</aside>

			</div>
			
		</div>
	</div>
	
</section>

<?php wp_reset_postdata(); ?>

<?php get_template_part( 'template-parts/mapa-locais' ); ?>

<?php get_footer();
