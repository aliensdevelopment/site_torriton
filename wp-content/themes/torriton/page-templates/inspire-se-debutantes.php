<?php
/*
Template Name: Inspire-se Debutantes
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-debutantes' ); ?>
</div>

<section id="debutantes" class="sub-page">

  <?php get_template_part( 'template-parts/debutantes/inspire-se-galeria' ); ?>

  <?php get_template_part( 'template-parts/debutantes/baile-debutantes' ); ?>
</section>

<?php get_footer();
