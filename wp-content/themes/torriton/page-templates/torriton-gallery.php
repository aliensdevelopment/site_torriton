<?php
/*
  Template Name: Torriton Gallery
*/
get_header(); ?>

<div class="container">
	<?php get_template_part( 'template-parts/shared/sidenav-gallery' ); ?>
</div>

<section id="sobre" class="container-spacing">
	<div class="grid-container full wrap-block-side torriton-gallery-wrapper">
		<div data-aos="fade-left" data-aos-duration="900" style="background-image: url('<?php bloginfo('template_directory'); ?>/dist/assets/images/fotos/gallery/torriton-gallery-01.jpg')" class="foto-bg estetico-torriton-bg"></div>
		<div class="grid-container wrap-block-side transparent full-height">
			<div class="grid-x grid-margin-x align-center block-content">
				<div class="medium-5 small-12">
					<h1 data-aos="fade-right" class="small-10 cell t-title-diamond estetico-torriton-title">
						Torriton Gallery
					</h1>
					<div class="grid-x grid-padding-x block-dicas-bgwhite align-justify">
						<div class="box-info-int box-info-dica cell small-12">
							<span class="line"></span>
							<p class="t-info-text">
								Ir as compras sem precisar sair do salão? Parece um sonho? Aqui é realidade!
								Conheça as lojas que fazem parte do Torriton Gallery!
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div id="baoo" class="grid-container full wrap-block-side full-height">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class=" small-12 medium-12">
				<div class="grid-x grid-padding-x ">
					<div class="medium-12 medium-offset-4 align-right bloco-wish">
						<div id="" class="text-area">
							<h1 data-aos="fade-right" class="text-left t-title-diamond ">Baoo</h1>
							<p class="t-info-text">
								Enquanto você aguarda hooooooras com os “bobs” na cabeça você pode dar
								uma passadinha na BAOO – Loja de Roupas arrasadoras!
							</p>
							<p class="t-social">
								Siga nas redes sociais.
							</p>
							<a href="https://www.facebook.com/BaooStore/" target="_blank">
							<i><?php get_template_part('dist/assets/images/icon', 'fb.svg'); ?></i>
							</a>

							<a href="https://www.instagram.com/baoostore/?hl=en" target="_blank">
								<i><?php get_template_part('dist/assets/images/icon', 'instagram.svg'); ?></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_template_part( 'template-parts/galerias/galeria-foto' ); ?>

<section data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">
		<div class="galeria galeria-baoo int carrosel owl-carousel owl-theme ">
        <?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-baoo' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                <?php 

                $images = get_field('galeria');

                if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                            <div class="galeria-item">
                              <img onclick="galeriaModal(this)" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />
                             </div>
                        <?php endforeach; ?>
                <?php endif; ?>

            <?php endforeach; 
            // Fim do Laço
          wp_reset_postdata();?>
		</div>
</section>

<div id="cor-e-casa" class="grid-container full wrap-block-side full-height">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class=" small-12 medium-12">
				<div class="grid-x grid-padding-x ">
					<div class="medium-12 medium-offset-4 align-right bloco-wish">
						<div id="" class="text-area">
							<h1 data-aos="fade-right" class="text-left t-title-diamond ">Cor e Casa</h1>
							<p class="t-info-text">
								A Cor e Casa surgiu da vontade de receber bem amigos e encanta-los com
								mesas postas pela própria Renata Martinez, proprietária da loja! A loja possui
								instagram com peças de decoração lindíssimas e montagens de mesa
								inspiradoras! Também conta com um site e agora com um novo ambiente
								dentro do Torriton Gallery.
							</p>
							<p class="t-social">
								Siga nas redes sociais.
							</p>
							<a href="https://www.facebook.com/corecasabyrm/" target="_blank">
							<i><?php get_template_part('dist/assets/images/icon', 'fb.svg'); ?></i>
							</a>

							<a href="https://www.instagram.com/corecasa/?hl=en " target="_blank">
								<i><?php get_template_part('dist/assets/images/icon', 'instagram.svg'); ?></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_template_part( 'template-parts/galerias/galeria-foto' ); ?>

<section data-aos="fade-left" data-aos-delay="400" data-aos-duration="900"  class="grid-container full">
		<div class="galeria galeria-baoo int carrosel owl-carousel owl-theme ">
        <?php
            global $post;
            $args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category_name' => 'galeria-cor-e-casa' );

            $myposts = get_posts( $args );

            // Início do Laço selecionado pelo nome da categoria
            foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

                <?php 

                $images = get_field('galeria');

                if( $images ): ?>
                        <?php foreach( $images as $image ): ?>
                            <div class="galeria-item">
                              <img onclick="galeriaModal(this)" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="thumbnail" />
                             </div>
                        <?php endforeach; ?>
                <?php endif; ?>

            <?php endforeach; 
            // Fim do Laço
          wp_reset_postdata();?>
		</div>
</section>

<?php get_template_part( 'template-parts/blog/blog-horario' ); ?>

<?php get_template_part( 'template-parts/nails/mapa-nails' ); ?>

<?php get_footer();