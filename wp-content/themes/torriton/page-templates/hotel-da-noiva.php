<?php
/*
Template Name: Hotel dia da Noiva
*/
get_header(); ?>

<div class="container">
  <?php get_template_part( 'template-parts/shared/sidenav-noivxs' ); ?>
</div>

<section id="noivas-e-noivos" class="sub-page">

  <?php get_template_part( 'template-parts/noivas-noivos/hotel-noiva' ); ?>
</section>

<?php get_footer();
