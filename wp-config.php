<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'torriton_dev');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+1{IqL`D5-Xl@kvq-,L*5,pI#s7nn%Ki#w&SRZ|s6aT4uh3PK;~qM!GCQ9iql*c_');
define('SECURE_AUTH_KEY',  ';j/S8*k-04(<d$}yj~8-xF&<=C+][=^rl`KR%#aJPyg1adRZA|!x376cTkn@s:?y');
define('LOGGED_IN_KEY',    '-y!K/qu[BSi=?3-rh`:DI/=TgFf~B*JGTO-K;#G+W]sNrBYHJOgev=Yimkr.Tn;4');
define('NONCE_KEY',        'A/k=KcN@VfqVa@{iLZZiO%?H[5m(q9P0vU|+09J|K9?,-[Ay4JIFyq1bxnq?z?$X');
define('AUTH_SALT',        'h .=yHa{h=]Nq=R3BaH=VTV1usTf7s2tc6km},}df[IZPr8DXx l#~8U/^r%J(Ku');
define('SECURE_AUTH_SALT', '0<%_/;/1J7(}~aEt0n2i5>d)?_/dj.O8Ue`]o2,R~//N 0R)gf@G~deLU,Xn/]_X');
define('LOGGED_IN_SALT',   '99P[je-<Tj23*gG<V[$]%lZzao#NtId%zSxq}DEb.v}oEXTuACM7:hV6ypXdNu6O');
define('NONCE_SALT',       '!$1$(Bm5I7S9i7Rx#Za!)3dh2^z [|{Tuw2[LI0O@Q#jK;wr~u$)Dd=bRXbP^!X(');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
